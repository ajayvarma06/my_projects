package com.epam.cloud.tests;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import com.epam.cloud.factory.DriverManagerFactory;
import com.epam.cloud.pages.GoogleCloudHomePage;
import com.epam.cloud.pages.PricingCalculatorPage;
import com.epam.cloud.pages.SearchResultPage;

public abstract class BaseTest {
	public  GoogleCloudHomePage googleCloudHomePage;
	public  SearchResultPage searchResultPage;
	public  PricingCalculatorPage calculatorPage;
	public WebDriver ddriver;
	
	@BeforeTest
	public void setUp() throws IOException {
		ddriver=DriverManagerFactory.getTheDriver().getDriver();
		ddriver.manage().window().maximize();
		calculatorPage=new PricingCalculatorPage(ddriver);
		googleCloudHomePage=new GoogleCloudHomePage(ddriver);
		searchResultPage = new SearchResultPage(ddriver);
	}
	
	@AfterTest
	public void tearDown() {
		ddriver.close();
	}
}
