package com.epam.cloud.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.epam.cloud.utility.CalculatorResult;
import com.epam.cloud.utility.Utility;

public class GoogleCloudPricingTest extends BaseTest {
	
	@Test(priority = -1)
	public void testWebsite() {
		if (googleCloudHomePage != null) {
		googleCloudHomePage.getHomePage();
		}
		Assert.assertEquals(googleCloudHomePage.currentPageURL(),Utility.GOOGLE_CLOUD_HOMEPAGE_URL);
	}
	@Test(priority = 0 , dependsOnMethods = "testWebsite")
	public void testSearchResultPage() {
		searchResultPage =	googleCloudHomePage.searchClick();
	}
	
	@Test(priority = 1 , dependsOnMethods = "testSearchResultPage")
	public void testSearchResultFunctionality() throws InterruptedException  {
		calculatorPage = searchResultPage.navigateToCalculatorPage();
		Assert.assertEquals(calculatorPage.currentPageURL(), Utility.PRICING_CALCULATOR_URL);
	}
	
	@Test(priority = 2,dependsOnMethods = "testSearchResultFunctionality")
	public void testFillingForm() { 
		calculatorPage.switchFrame();
		calculatorPage.choseComputeEngine();
		calculatorPage.enterNoOfInstances();
		calculatorPage.chooseOperatingSystem();
		calculatorPage.chooseProvisioningModel();
		calculatorPage.chooseSeries();
		calculatorPage.chooseInstanceType();
		calculatorPage.selectCheckBoxGPU();
		calculatorPage.chooseGPUType();
		calculatorPage.chooseNoOfGPU();
		calculatorPage.chooseLocalSSD();
		calculatorPage.chooseLocation();
		calculatorPage.chooseCommitedUsage();
		calculatorPage.clickEstimateButton();
		
	}

	@Test(priority = 3 ,dependsOnMethods = "testFillingForm", enabled = true)
	public void testAfterEstimate() {
		SoftAssert softAssert=new SoftAssert();
		softAssert.assertEquals(calculatorPage.getRegion(),CalculatorResult.REGION);
		softAssert.assertEquals(calculatorPage.getCommitmentTerm(), CalculatorResult.COMMITTED_USAGE);
		softAssert.assertEquals(calculatorPage.getProvisioningModel(),CalculatorResult.MODEL);
		softAssert.assertEquals(calculatorPage.getInstanceType(),CalculatorResult.INSTANCE_TYPE);
		softAssert.assertEquals(calculatorPage.getLocalSSD(),CalculatorResult.LOCAL_SSD);
		softAssert.assertEquals(calculatorPage.getEstimatedCost(),CalculatorResult.OVERALL_ESTIMATED_COST);
		softAssert.assertAll();
	}
	
}
