package com.epam.cloud.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PricingCalculatorPage extends BasePage {
	
	public PricingCalculatorPage(WebDriver driver) {
		super(driver);		
	}
	
	@FindBy(xpath = "//devsite-iframe//iframe")
	private WebElement mainFrame;
	@FindBy(xpath = "//iframe[@id='myFrame']")
	private WebElement subframe;
	private By computeEngine=By.xpath("//md-tab-item/div[@title='Compute Engine']");
	private By noOfInstance=By.xpath("//input[@ng-model='listingCtrl.computeServer.quantity']");
	private By chooseOS=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.os']");
	private By valuesOS=By.xpath("//div[@class='md-select-menu-container md-active md-clickable']/md-select-menu/md-content/md-option");
	private By model=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.class']");
	private By modelBox=By.xpath("//div[@class='md-select-menu-container md-active md-clickable']/md-select-menu/md-content/md-option[1]");
	private By series=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.series']");
	private By seriesBox = By.xpath("//div[@class='md-select-menu-container md-active md-clickable']//md-select-menu/md-content/md-option");
	private By machineType=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.instance']");
	private By machineTypeBox=By.xpath("//md-optgroup[@label='standard']/md-option");
	private By checkBoxGPU=By. xpath("//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']");	
	private By gpuType=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.gpuType']");
	private By gpuTypeBox=By.xpath("//div[@class='md-select-menu-container md-active md-clickable']/md-select-menu/md-content/md-option");
	private By noOfGPUs=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.gpuCount']");
	private By noOfGPUBox=By.xpath("//div[@class='md-select-menu-container md-active md-clickable']/md-select-menu/md-content/md-option");
	private By localssd=By.xpath("//md-select[@ng-model='listingCtrl.computeServer.ssd']");
	private By localSSDBox=By.xpath("//div[@class='md-select-menu-container md-active md-clickable']/md-select-menu/md-content/md-option");
	private By location= By.xpath("//md-select[@ng-model='listingCtrl.computeServer.location']");
	private By locationBox=By.xpath("//md-option[@ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer']");
	private By commitedUsage= By.xpath("//md-select[@ng-model='listingCtrl.computeServer.cud']");
	private By commitedUsageBox=By.xpath("//div[@class='md-select-menu-container md-active md-clickable']/md-select-menu/md-content[@class='_md']/md-option");
	private By estimateButton=By.xpath("//div[@class='layout-align-end-start layout-row']/button");

	
	public void switchFrame() {
		driver.switchTo().frame(mainFrame);
		driver.switchTo().frame(subframe);
	}
	
	public void choseComputeEngine() {
		driver.findElement(computeEngine).click();
	}
	
	public void enterNoOfInstances() {
		WebElement instancElement= driver.findElement(noOfInstance);
		instancElement.click();
		instancElement.sendKeys("4");
	}
	
	public void chooseOperatingSystem() {
		WebElement os= driver.findElement(chooseOS);
		os.click();
		List<WebElement> listOfOS=driver.findElements(valuesOS);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfOS.get(0)));
		listOfOS.get(0).click();
	}
	

	public void chooseProvisioningModel() {
		WebElement modelElement =driver.findElement(model);
		modelElement.click();
		List<WebElement> listOfModels=driver.findElements(modelBox);
		//WebElement listOfModels =driver.findElement(modelBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfModels.get(0)));
		listOfModels.get(0).click();
	}
	
	public void chooseSeries() {
		WebElement seriesElement =driver.findElement(series);
		seriesElement.click();
		List<WebElement> listOfSeries=driver.findElements(seriesBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfSeries.get(0)));
		listOfSeries.get(0).click();
	}

	public void chooseInstanceType() {
		WebElement machine =driver.findElement(machineType);
		machine.click();
		List<WebElement> listOfMachines=driver.findElements(machineTypeBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfMachines.get(3)));
		listOfMachines.get(3).click();
	}

	public void selectCheckBoxGPU() {
		driver.findElement(checkBoxGPU).click();		
	}
	
	public void chooseGPUType() {
		WebElement gpu=driver.findElement(gpuType);
		gpu.click();
		List<WebElement> listOfGPUs=driver.findElements(gpuTypeBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfGPUs.get(3)));
		listOfGPUs.get(3).click();
	}
	
	public void chooseNoOfGPU()  {
		WebElement noOfGPU=driver.findElement(noOfGPUs);
		noOfGPU.click();
		List<WebElement> listOfNoOfGPUs=driver.findElements(noOfGPUBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfNoOfGPUs.get(1)));
		listOfNoOfGPUs.get(1).click();
	}
	
	public void chooseLocalSSD() {
		WebElement ssd=driver.findElement(localssd);
		ssd.click();
		List<WebElement> listOfSSDs=driver.findElements(localSSDBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfSSDs.get(2)));
		listOfSSDs.get(2).click();
	}
	
	public void chooseLocation() {
		WebElement locationElement=driver.findElement(location);
		locationElement.click();
		List<WebElement> listOfLocations=driver.findElements(locationBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfLocations.get(25)));
		listOfLocations.get(25).click();
	}
	
	public void chooseCommitedUsage() {
		WebElement usage=driver.findElement(commitedUsage);
		usage.click();
		List<WebElement> listOfCommitedUsage=driver.findElements(commitedUsageBox);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(listOfCommitedUsage.get(1)));
		listOfCommitedUsage.get(1).click();
	}
	
	public void clickEstimateButton() {
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(estimateButton));
		driver.findElement(estimateButton).click();
	}
	
//After Estimate
	@FindBy(xpath="//md-list[@class='cartitem ng-scope']/md-list-item[1]/div[1]")
	private WebElement region;
	@FindBy(xpath="//md-list[@class='cartitem ng-scope']/md-list-item[3]/div[1]")
	private WebElement commitmentTerm;
	@FindBy(xpath="//md-list[@class='cartitem ng-scope']/md-list-item[4]/div[1]")
	private WebElement provisioningModel;
	@FindBy(xpath="//md-list[@class='cartitem ng-scope']/md-list-item[5]/div")
	private WebElement instanceType;
	@FindBy(xpath="//md-list[@class='cartitem ng-scope']/md-list-item[7]/div")
	private WebElement localSSD;
	@FindBy(xpath="//md-list-item/div/b")
	private WebElement cost;
	
	
	public String getRegion() {
		return region.getText();  
	}
	
	public String getCommitmentTerm() {
		return commitmentTerm.getText();	
	}
	
	public String getProvisioningModel() {
		return provisioningModel.getText();
	}
	
	public String getInstanceType() {
		return instanceType.getText(); 
	}
	
	public String getLocalSSD() {
		return localSSD.getText();		
	}
	
	public String getEstimatedCost() {
		return cost.getText();
	}
}
