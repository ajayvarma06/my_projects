package com.epam.cloud.pages;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultPage extends BasePage{
	
	public SearchResultPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "(//a[@class='gs-title'])[1]")
	private WebElement firstResult;
			
	public PricingCalculatorPage navigateToCalculatorPage(){
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOf(firstResult));
		firstResult.click();
		return new PricingCalculatorPage(driver);
	}
	
}
