package com.epam.cloud.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Log {

    private Log() {
    }

    private static final Logger LOGGER = LogManager.getLogger("binary_data_logger");

    public static void logInfoMessage(String message) {
        LOGGER.info(message);
    }

    public static void logInfoMessage(String msg, Object... params) {
        LOGGER.info(String.format(msg, params));
    }

    public static void logInfoMessage(File file, String message) {
        LOGGER.info(String.format("RP_MESSAGE#FILE#%s#%s", file.getAbsolutePath(), message));
    }

    public static void logDebug(String message) {
        LOGGER.debug(message);
    }
}