package com.epam.cloud.driverManagers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class MyFirefoxWithCapabilities extends DriverManager {
	public MyFirefoxWithCapabilities() {
		FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        driver= new FirefoxDriver(options);

	}

}
