package com.epam.cloud.utility;

import java.util.Properties;

public class CalculatorResult {
	private static Properties properties;
	static {
		try {
			properties=FileReader.readPropertiesFile("src/main/resources/CalculatorResult.properties");
		}
		catch (Exception e) {
			throw new RuntimeException();
		}
	}
	public static final String REGION=properties.getProperty("Region");
	public static final String COMMITTED_USAGE=properties.getProperty("CommittedUsage");
	public static final String MODEL=properties.getProperty("Model");
	public static final String INSTANCE_TYPE=properties.getProperty("InstanceType");
	public static final String LOCAL_SSD=properties.getProperty("LocalSSD");
	public static final String OVERALL_ESTIMATED_COST=properties.getProperty("OverallEstimatedCost");

}
