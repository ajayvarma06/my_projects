package com.epam.BuiderDesignTask;

public class Bicycle {
    boolean hasGears;
    boolean hasDoubleStand;
    boolean hasDoubleSeat;
    boolean hasCarrier;
    int noOfGears;
    int noOfcarrier;
    public static BicycleBuilder builder()
	{
		return new BicycleBuilder();
	}

    static class BicycleBuilder
	{ 
    	boolean hasGears;
        boolean hasDoubleStand;
        boolean hasDoubleSeat;
        boolean hasCarrier;
        int noOfGears;
        int noOfcarrier;
		public BicycleBuilder hasGears(boolean hasGears)
		{
			this.hasGears=hasGears;
			return this;
		}
		public BicycleBuilder hasDoubleStand(boolean hasDoubleStand)
		{
			this.hasDoubleStand=hasDoubleStand;
			return this;
		}
		public BicycleBuilder hasDoubleSeat(boolean hasDoubleSeat)
		{
			this.hasDoubleSeat=hasDoubleSeat;
			return this;
		}
		public BicycleBuilder hasCarrier(boolean hasCarrier)
		{
			this.hasCarrier=hasCarrier;
			return this;
		}
		public BicycleBuilder noOfcarrier(int noOfcarrier)
		{
			this.noOfcarrier=noOfcarrier;
			return this;
		}
		public BicycleBuilder noOfGears(int noOfGears)
		{
			this.noOfGears=noOfGears;
			return this;
		}
		
		public Bicycle build()
		{
			Bicycle bicycleObject=new Bicycle();
			if(hasGears)
			{
				bicycleObject.noOfGears=noOfGears;	
			}
			if(hasCarrier)
			{
				bicycleObject.noOfcarrier=noOfcarrier;
			}
			bicycleObject.hasGears=hasGears;
			bicycleObject.hasDoubleSeat=hasDoubleSeat;
			bicycleObject.hasDoubleStand=hasDoubleStand;
			bicycleObject.hasCarrier=hasCarrier;
			return bicycleObject;
		}
	}
}