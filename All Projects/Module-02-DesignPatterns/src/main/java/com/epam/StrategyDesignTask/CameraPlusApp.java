package com.epam.StrategyDesignTask;

public class CameraPlusApp implements PhoneCameraApp{
    @Override
    public Photo takePhoto() {
        System.out.println("Photo Taken from Camera Plus App");
        return new Photo();
    }

    @Override
    public void savePhoto(Photo photo) {
        System.out.println("Photo Saved from Camera Plus App");
    }

    @Override
    public void editor(Photo photo) {
        System.out.println("Here photos can be edited using Plus Editor");
    }
}
