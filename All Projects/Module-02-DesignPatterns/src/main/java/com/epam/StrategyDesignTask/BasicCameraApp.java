package com.epam.StrategyDesignTask;

public class BasicCameraApp implements PhoneCameraApp{
    @Override
    public Photo takePhoto() {
        System.out.println("Photo Taken from Basic Camera App");
        return new Photo();
    }

    @Override
    public void savePhoto(Photo photo) {
        System.out.println("Photo Saved from Basic Camera App");
    }

    @Override
    public void editor(Photo photo) {
        System.out.println("Here photos can be edited using Basic Editor");
    }
}