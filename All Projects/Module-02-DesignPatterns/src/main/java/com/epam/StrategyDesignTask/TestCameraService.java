package com.epam.StrategyDesignTask;

public class TestCameraService {

	public static void main(String[] args) {
		ShareService shareService1 = new ShareService();
        ShareService shareService2 = new ShareService();
        shareService1.setStrategy(new ShareByEmail());
        shareService2.setStrategy(new ShareByTextSMS());

        BasicCameraApp basicCamera = new BasicCameraApp();
        CameraPlusApp cameraPlus = new CameraPlusApp();

        System.out.println("Testing Basic Camera App");
        Photo photo1 = basicCamera.takePhoto();
        basicCamera.savePhoto(photo1);
        basicCamera.editor(photo1);
        shareService1.sharePhotoViaAnyMedium(photo1);
        shareService2.sharePhotoViaAnyMedium(photo1);
        System.out.println();
        System.out.println("Testing Camera Plus App");
        Photo photo2 = cameraPlus.takePhoto();
        cameraPlus.savePhoto(photo2);
        cameraPlus.editor(photo2);
        shareService1.sharePhotoViaAnyMedium(photo2);
        shareService2.sharePhotoViaAnyMedium(photo2);
	}

}
