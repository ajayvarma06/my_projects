package com.epam.StrategyDesignTask;

public class ShareService {
    private ShareMethod strategy;

    public void setStrategy(ShareMethod shareStrategy) {
        this.strategy = shareStrategy;
    }

    public void sharePhotoViaAnyMedium(Photo photo) {
        strategy.share(photo);
    }
}
