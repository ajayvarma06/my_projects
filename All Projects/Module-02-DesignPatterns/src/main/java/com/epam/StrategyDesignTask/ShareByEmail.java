package com.epam.StrategyDesignTask;

public class ShareByEmail implements ShareMethod{
    @Override
    public void share(Photo photo) {
        System.out.println("Photo Shared via Email");
    }
}