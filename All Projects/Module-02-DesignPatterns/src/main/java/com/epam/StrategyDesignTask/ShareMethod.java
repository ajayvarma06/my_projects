package com.epam.StrategyDesignTask;

public interface ShareMethod {
    void share(Photo photo);
}