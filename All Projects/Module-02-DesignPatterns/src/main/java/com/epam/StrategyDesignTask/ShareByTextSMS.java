package com.epam.StrategyDesignTask;

public class ShareByTextSMS implements ShareMethod{
    @Override
    public void share(Photo photo) {
        System.out.println("Photo Shared via SMS");
    }
}
