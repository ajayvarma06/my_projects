package com.epam.DecoratorDesignTask;

public class Main {

	public static void main(String[] args) {
		Webpage basicWebPage= new BasicWebPage();
        System.out.println(basicWebPage.getRankSum());
        basicWebPage.homepage("new to programming");
        System.out.println();

        basicWebPage=new MobileVersion(basicWebPage);
        System.out.println(basicWebPage.getRankSum());
        basicWebPage.homepage("Inside Android");
        System.out.println();

        basicWebPage=new DesktopVersion(basicWebPage);
        System.out.println(basicWebPage.getRankSum());
        basicWebPage.homepage("inside hp");
        System.out.println();

        basicWebPage = new DateAndTimeWidget(basicWebPage);
        System.out.println(basicWebPage.getRankSum());
        basicWebPage.homepage("Displays date and time");

	}

}
