package com.epam.DecoratorDesignTask;


public class DecoratorWebPage implements Webpage {
    private Webpage webPage;

    public DecoratorWebPage(Webpage webpage)
    {
        this.webPage=webpage;
    }

    @Override
    public void homepage(String mode) {
        webPage.homepage(mode);
    }

    @Override
    public int getRankSum() {
        return webPage.getRankSum();
    }

}
