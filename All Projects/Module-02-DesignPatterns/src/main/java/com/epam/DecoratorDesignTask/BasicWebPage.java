package com.epam.DecoratorDesignTask;

public class BasicWebPage implements Webpage {
	int Rank=2;
    @Override
    public void homepage(String mode) {
        System.out.println("Welcome to Homepage " + mode);
    }
    @Override
    public int getRankSum() {
        return Rank;
    }

}