package com.epam.DecoratorDesignTask;

public class DesktopVersion extends DecoratorWebPage {
	int Rank=7;
    public DesktopVersion(Webpage webpage) {
        super(webpage);
    }

    @Override
    public void homepage(String mode) {
        super.homepage(mode);
        System.out.println("Works on Desktop");
    }

    @Override
    public int getRankSum() {
        return Rank+super.getRankSum();
    }
}
