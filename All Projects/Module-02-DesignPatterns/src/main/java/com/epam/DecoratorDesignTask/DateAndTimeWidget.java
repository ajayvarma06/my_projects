package com.epam.DecoratorDesignTask;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateAndTimeWidget extends DecoratorWebPage {
    Integer Rank=7;
    public DateAndTimeWidget(Webpage webpage) {
        super(webpage);
    }

    @Override
    public void homepage(String mode) {
        super.homepage(mode);
    }

    @Override
    public int getRankSum() {
        return Rank+super.getRankSum();
    }
}
