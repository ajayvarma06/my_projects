package com.epam.DecoratorDesignTask;

public interface Webpage {
    void homepage(String mode);
    int getRankSum();

}

