package com.epam.DecoratorDesignTask;

public class MobileVersion extends DecoratorWebPage {
	int Rank=4;
    public MobileVersion(Webpage webpage) {
        super(webpage);
    }

    @Override
    public void homepage(String mode) {
        super.homepage(mode);
        System.out.println("Works on Mobile");
    }

    @Override
    public int getRankSum() {
        return Rank+super.getRankSum();
    }

}
