package com.epam.SingletonDesignTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
	public static void createInstance(){
		CandyMaker cart = CandyMaker.getInstance();
        System.out.println(cart);
    }
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(Main::createInstance);
        service.submit(Main::createInstance);
        service.shutdown();

    }
}
