package com.epam.collections2Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployeeAnalyzer {
	public static int maleCount = 0;
	public static int femaleCount = 0;
	public static Set<String> allDepartments = new HashSet<String>();
	public static Employee highestPaidEmployee;
	public static List<String> employeesJoinedAfter2015 = new ArrayList<String>();
	public static HashMap<String, Integer> employeesInEachDepartment = new HashMap<>();
	public static HashMap<String, Double> avgSalaryOfDepartment = new HashMap<>();
	public static Employee youngestMaleInProductDevelopment;
	public static Employee mostExperiencedEmployee;
	public static List<Employee> ageLessOrEqual25 = new ArrayList<>();
	public static List<Employee> ageGreater25 = new ArrayList<>();

	public static void countGenders(List<Employee> employeeList) {
		for (Employee employee : employeeList) {
			if (employee.getGender().equals("Male")) {
				maleCount++;
			} else {
				femaleCount++;
			}
		}
		System.out.println("No. of Males: " + maleCount);
		System.out.println("No. of Females: " + femaleCount);
		System.out.println("------");
	}

	public static void listAllDepartments(List<Employee> employeeList) {
		for (Employee employee : employeeList) {
			allDepartments.add(employee.getDepartment());
		}
		System.out.println(allDepartments);
		System.out.println("------");
	}

	public static void calculateAvgMaleFemaleAge(List<Employee> employeeList) {
		int totalMaleAge = 0;
		int totalFemaleAge = 0;
		for (Employee employee : employeeList) {
			if (employee.getGender().equals("Male")) {
				totalMaleAge += employee.getAge();
			} else {
				totalFemaleAge += employee.getAge();
			}
		}
		System.out.println("Male Average Age: " + (totalMaleAge / maleCount));
		System.out.println("Female Average Age: " + (totalFemaleAge / femaleCount));
		System.out.println("------");
	}

	public static void findHighestPaidEmployee(List<Employee> employeeList) {
		highestPaidEmployee = Collections.max(employeeList, new Comparator<Employee>() {
			public int compare(Employee firstEmployee, Employee secondEmployee) {
				if (firstEmployee.getSalary() > secondEmployee.getSalary()) {
					return 1;
				} else if (firstEmployee.getSalary() < secondEmployee.getSalary()) {
					return -1;
				}
				return 0;
			}
		});
		System.out.println(highestPaidEmployee);
		System.out.println("------");
	}

	public static void findEmployeesJoinedAfter2015(List<Employee> employeeList) {
		for (Employee employee : employeeList) {
			if (employee.getYearOfJoining() > 2015) {
				employeesJoinedAfter2015.add(employee.getName());
			}
		}
		System.out.println(employeesJoinedAfter2015);
		System.out.println("------");
	}

	public static void countEmployeesInEachDepartment(List<Employee> employeeList) {
		for (Employee employee : employeeList) {
			String department = employee.getDepartment();
			if (employeesInEachDepartment.containsKey(department)) {
				employeesInEachDepartment.put(department, employeesInEachDepartment.get(department) + 1);
			} else {
				employeesInEachDepartment.put(department, 1);
			}
		}
		System.out.println(employeesInEachDepartment);
		System.out.println("------");
	}

	public static void calculateAvgSalaryOfDepartment(List<Employee> employeeList) {
		for (Employee employee : employeeList) {
			String department = employee.getDepartment();
			double salary = employee.getSalary();
			if (avgSalaryOfDepartment.containsKey(department)) {
				avgSalaryOfDepartment.put(department, avgSalaryOfDepartment.get(department) + salary);
			} else {
				avgSalaryOfDepartment.put(department, salary);
			}
		}
		for (String department : avgSalaryOfDepartment.keySet()) {
			avgSalaryOfDepartment.put(department,
					avgSalaryOfDepartment.get(department) / employeesInEachDepartment.get(department));
		}
		System.out.println(avgSalaryOfDepartment);
		System.out.println("------");
	}

	public static void findYoungestMaleInProductDevelopment(List<Employee> employeeList) {
		youngestMaleInProductDevelopment = null;
		for (Employee employee : employeeList) {
			if (employee.getGender().equals("Male") && employee.getDepartment().equals("Product Development")) {
				if (youngestMaleInProductDevelopment == null) {
					youngestMaleInProductDevelopment = employee;
				} else if (employee.getAge() < youngestMaleInProductDevelopment.getAge()) {
					youngestMaleInProductDevelopment = employee;
				}
			}
		}
		System.out.println(youngestMaleInProductDevelopment);
		System.out.println("------");
	}

	public static void findMostExperiencedEmployee(List<Employee> employeeList) {
		mostExperiencedEmployee = Collections.min(employeeList, new Comparator<Employee>() {
			@Override
			public int compare(Employee firstEmployee, Employee secondEmployee) {
				if (firstEmployee.getYearOfJoining() < secondEmployee.getYearOfJoining()) {
					return -1;
				} else if (firstEmployee.getYearOfJoining() > secondEmployee.getYearOfJoining()) {
					return 1;
				} else {
					return 0;
				}
			}
		});
		System.out.println(mostExperiencedEmployee);
		System.out.println("------");
	}

	public static void countMaleFemaleInSalesAndMarketing(List<Employee> employeeList) {
		int maleCountInSalesAndMarketing = 0;
		int femaleCountInSalesAndMarketing = 0;
		for (Employee employee : employeeList) {
			if (employee.getDepartment().equals("Sales And Marketing")) {
				if (employee.getGender().equals("Male")) {
					maleCountInSalesAndMarketing++;
				} else {
					femaleCountInSalesAndMarketing++;
				}
			}
		}
		System.out.println("No. of Males in Sales and Marketing: " + maleCountInSalesAndMarketing);
		System.out.println("No. of Females in Sales and Marketing: " + femaleCountInSalesAndMarketing);
		System.out.println("------");
	}

	public static void calculateAvgMaleFemaleSalary(List<Employee> employeeList) {
		double totalMaleSalary = 0.0;
		double totalFemaleSalary = 0.0;
		int maleCount = 0;
		int femaleCount = 0;
		for (Employee employee : employeeList) {
			if (employee.getGender().equals("Male")) {
				totalMaleSalary += employee.getSalary();
				maleCount++;
			} else {
				totalFemaleSalary += employee.getSalary();
				femaleCount++;
			}
		}
		System.out.println("Male Average Salary: " + (totalMaleSalary / maleCount));
		System.out.println("Female Average Salary: " + (totalFemaleSalary / femaleCount));
		System.out.println("------");
	}

	public static void getEmpNameBasedOnDept(List<Employee> employeeList) {
		Set<String> depList = allDepartments;
		for (String department : depList) {
			System.out.println(department + " - Employees:");
			int count = 1;
			for (Employee employee : employeeList) {
				if (department.equals(employee.getDepartment())) {
					System.out.println(count + "." + employee.getName());
					count++;
				}
			}
			System.out.println();
		}
		System.out.println("------");
	}

	public static void getAvgAndWholeSalary(List<Employee> employeeList) {
		int total_employees = maleCount + femaleCount;
		double total_salary = 0.0;

		for (Employee employee : employeeList) {
			total_salary += employee.getSalary();
		}

		if (total_employees == 0) {
			System.out.println("There are no employees in the organization.");
		} else {
			System.out.println("Total Salary of Organization :" + (int) total_salary);
			System.out.println("Average Salary of Organization :" + (int) (total_salary) / total_employees);
		}
		System.out.println("------");
	}

	public static void empDetailsBasedOnAge25(List<Employee> employeeList) {
		for (Employee employee : employeeList) {
			if (employee.getAge() <= 25) {
				ageLessOrEqual25.add(employee);
			} else {
				ageGreater25.add(employee);
			}
		}
		System.out.println("Employees age less or equal to 25 :" + ageLessOrEqual25);
		System.out.println("Employees age greater than 25 :" + ageGreater25);
		System.out.println("------");
	}

	public static void oldestEmployeeDetails(List<Employee> employeeList) {
		Employee oldestEmp = employeeList.get(0);
		for (Employee employee : employeeList) {
			if (employee.getAge() > oldestEmp.getAge()) {
				oldestEmp = employee;
			}
		}
		System.out.println("Name of oldest employee :" + oldestEmp.getName() + "," + "Age :" + oldestEmp.getAge() + ","
				+ "Department :" + oldestEmp.getDepartment());
	}

}
