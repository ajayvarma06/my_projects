use world;


select * from (select * from country order by Name DESC limit 10)t1 order by t1.Name;


select c.Name AS City_Name,co.Name AS Country_Name,co.Population AS Country_Population
FROM city c inner join country co 
on c.CountryCode=co.Code
order by co.Population desc limit 1;


select * from country where Name LIKE '%Island%' AND LifeExpectancy>70;


select Language , count(*) as Num_Of_Countries from countrylanguage group by Language;


select count(*) as Num_Of_Countries from country where IndepYear>1971;


select c.Name as City_Name, c.Population as City_Population,
co.Name as Country_Name, co.HeadOfState as HeadOfState
from city c 
inner join country co 
on c.CountryCode=co.Code;


select co.HeadOfState,count(*) as Num_Of_Cities
from city c 
inner join country co
on c.CountryCode=co.Code
group by co.HeadOfState;


create view details as
select ci.Name as cityName, c.Name as countryName, c.Continent, c.HeadOfState, cl.Language from 
city ci join country c on ci.CountryCode=c.Code 
join countrylanguage cl on c.Code= cl.CountryCode;
select * from details;


alter view details as
select ci.Name as cityName, c.Name as countryName, c.Continent, c.HeadOfState,c.GovernmentForm, cl.Language from 
city ci join country c on ci.CountryCode=c.Code 
join countrylanguage cl on c.Code= cl.CountryCode;
select * from details;

