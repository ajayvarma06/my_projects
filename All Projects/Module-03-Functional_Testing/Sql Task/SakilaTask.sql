use sakila;

create table employee_details(
Emp_Id INT NOT NULL,
Emp_Name Varchar(50),
Emp_Designation Varchar(50) NOT NULL,
Emp_Location Varchar(50) default 'India',
Emp_Salary decimal(10,2) NOT NULL
);

alter table employee_details
add Emp_Department varchar(50),
add constraint pk_employee_details primary key(Emp_Id);

INSERT INTO employee_details
VALUES 
(1, 'John', 'Manager', 'Pak', 80000),
(2, 'Smith', 'Software Developer', 'Bal', NULL),
(3, 'Bob', NULL, 'Nepal', 55000),
(4, 'Mike', 'Sales Manager', 'China', 70000),
(5, 'Lisa', 'Marketing Manager', 'USA', 60000);

-- This will violate the Emp_Id unique constraint
INSERT INTO employee_details 
VALUES 
(1, 'Mark', 'Manager', 'India', 85000);

-- This will violate the Emp_Salary NOT NULL constraint
INSERT INTO employee_details 
VALUES 
(6, 'Wilson', 'Business Analyst', 'USA', NULL);

-- This will not violate any constraints
INSERT INTO employee_details
VALUES 
(6, 'Wilson', 'Business Analyst', 'China', 65000);