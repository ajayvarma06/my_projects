package com.epam.LiskovsSubstitutionPrinciple;

public class Ipod extends Mobile implements MusicPlayerManager {

	@Override
	public void playMusic(String fileName) {
		System.out.println("Playing music "+fileName);
		
	}

}