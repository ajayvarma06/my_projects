package com.epam.InterfaceSegregationPrinciple;

public interface CashBack {
	
    public void getCashBackAsCreditBalance();

}
