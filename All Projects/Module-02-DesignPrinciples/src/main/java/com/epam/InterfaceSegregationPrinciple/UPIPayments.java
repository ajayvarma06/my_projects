package com.epam.InterfaceSegregationPrinciple;


public interface UPIPayments {
	

	public void getScratchCard();
	
	public void getCashBackAsCreditBalance();
	
	public void payMoney();
		

    
}
