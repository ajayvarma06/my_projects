package com.epam.DependencyInversionPrinciple;

public interface PaymentMethod {
	
	public void doTransaction(int amount);


}
