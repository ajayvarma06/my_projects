package com.epam.DependencyInversionPrinciple;

public class TicketBookingApp {

	private PaymentMethod paymentMethod ;
	
	public TicketBookingApp(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public void doPayment(int noOfTickets, int amount) {
		paymentMethod.doTransaction(amount);
	}
	
	public static void main(String[] args) {
//		DebitCard debitCard = new DebitCard();
//		CreditCard creditCard =new CreditCard();
		
		PaymentMethod paymentMethod=new DebitCard();
		TicketBookingApp ticketApp = new TicketBookingApp(paymentMethod);
		ticketApp.doPayment(4,5000);
		
		paymentMethod =new CreditCard();
		ticketApp=new TicketBookingApp(paymentMethod);
		ticketApp.doPayment(6, 20000);
		
	}
}

