package com.epam.DependencyInversionPrinciple;

public class DebitCard implements PaymentMethod {
	
	public void doTransaction(int amount) {
		System.out.println("tx done with DebitCard");
	}
}
