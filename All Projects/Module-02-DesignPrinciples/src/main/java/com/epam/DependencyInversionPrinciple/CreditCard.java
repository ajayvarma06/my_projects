package com.epam.DependencyInversionPrinciple;


public class CreditCard implements PaymentMethod {
	
	public void doTransaction(int amount) {
		System.out.println("tx done with CreditCard");
	}
}
