package com.epam.GETHomeTask;

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
public class GetHomeTask {  
	
	 
	@Test    
	public void validateStatus() {        
		RestAssured.baseURI = "https://jsonplaceholder.typicode.com/users";        
		RequestSpecification request = RestAssured.given();        
		Response response = request.get();        
		response.prettyPrint();        
		Assert.assertEquals(response.getStatusCode(), 200);    
		}    
	
	@Test    
	public void testMoreThan3Users() {       
		RestAssured.baseURI = "https://jsonplaceholder.typicode.com/users";        
		RequestSpecification requestSpecification = RestAssured.given();       
		Response response = requestSpecification.get();        
		JSONArray jsonArray = new JSONArray(response.asPrettyString());        
		System.out.println(jsonArray.length());        
		//response.prettyPrint();        
		Assert.assertEquals(jsonArray.length() > 3, true);
		System.out.println("More Than 3 Users Found");
		}    
	
	@Test    
	public void isPresent() {        
		String name = "Ervin Howell";        
		RestAssured.baseURI = "https://jsonplaceholder.typicode.com/users";        
		RequestSpecification requestSpecification = RestAssured.given();        
		Response response = requestSpecification.request(Method.GET);       
		JSONArray jsonArray = new JSONArray(response.asPrettyString()); 
		System.out.println(jsonArray.length());
		boolean flag=false;        
		for (Iterator<Object> iterator = jsonArray.iterator(); iterator.hasNext(); ) {
			JSONObject element = (JSONObject) iterator.next();           
			if (element.get("name").equals("Ervin Howell")) {               
				flag=true;
				System.out.println(name +" : User Found");
				break;
				}       
			}        
		Assert.assertTrue(flag);    
		}
}





