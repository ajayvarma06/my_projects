package com.epam.Authorization;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*; 
public class Authorization 
{
    @Test
    public void authorization()
    {
    	given().auth()
    			.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")
    			.get("https://gorest.co.in/public/v2/users")
    			.then()
    			.statusCode(200);
    }
}
