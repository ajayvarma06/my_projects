package com.epam.Parameterization;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.*;
import static io.restassured.RestAssured.given;
import static java.lang.Thread.sleep;

public class parameterization {
	public static int id =0;
	@Test
    public void verifyUsersCount(){
    	Response response=given()
    			.baseUri("https://gorest.co.in/public/v2")
    			.when()
    			.auth()                
				.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538") 
    			.get("/users");
    	response.prettyPrint();
    	
    	int empCount=response.jsonPath().getList("data").size(); 
    	System.out.println(empCount);
    	Assert.assertEquals(empCount, 10);
    	System.out.println(response.statusCode());
//    	JSONArray jsonArray = new JSONArray(response.as(List.class));
//		jsonArray.forEach(System.out::println);        
//		System.out.println(jsonArray.length());
    	response.then()
    	.statusCode(200)
    	.header("Content-Encoding", "gzip")
		.header("Content-type","application/json; charset=utf-8")
		.statusLine("HTTP/1.1 200 OK");
    }
	
	@Test    
	public void createNewUser()  {        
		RestAssured.baseURI="https://gorest.co.in/public/v2";
		String employeeData= "{\r\n"
    			+ "        \"id\": 10095,\r\n"
    			+ "        \"name\": \"Ajay\",\r\n"
    			+ "        \"email\": \"ajayvarma@epam.info\",\r\n"
    			+ "        \"gender\": \"male\",\r\n"
    			+ "        \"status\": \"active\"\r\n"
    			+ "    }";        
		Response employeeCreatedResponse = given()                
				.when().auth()                
				.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")                
				.header("Content-type","application/json")                
				.body(employeeData)                
				.post("/users");        
		employeeCreatedResponse.prettyPrint(); 
		id=employeeCreatedResponse.then().contentType(ContentType.JSON).extract().path("id");
		System.out.println(id);
		employeeCreatedResponse.then().statusCode(201)
		.header("Content-type","application/json; charset=utf-8")
		.statusLine("HTTP/1.1 201 Created");
		//After Adding Data: Verification		
		Response afterAdd= given()
				.when().auth()                
				.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")                
				.get("/users");        
		afterAdd.prettyPrint();        
		int empCount=afterAdd.jsonPath().getList("data").size(); 
    	System.out.println(empCount);
    	afterAdd.then()
    	.statusCode(200)
    	.header("Content-Encoding", "gzip")
		.header("Content-type","application/json; charset=utf-8")
		.statusLine("HTTP/1.1 200 OK");
		}
		
	
	@Test    
	void verifyUpdatedData()    {        
		String newID=Integer.toString(id);
	Response response=given()                
		.auth()                
		.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")                
		.get("https://gorest.co.in/public/v2/users/1322961");
	response.prettyPrint();
	response.then().assertThat()
	.body("name", equalTo("Ajay"))
	.body("email", equalTo("ajayvarma@epam.info"))
	.body("gender", equalTo("male"))
	.body("status", equalTo("active"))
	.statusCode(200)
	.header("Content-Encoding", "gzip")
	.header("Content-type","application/json; charset=utf-8")
	.statusLine("HTTP/1.1 200 OK");
		}
	
	@Test    
	void updateEmployee()    {        
		String employeeData= "{\r\n"
    			+ "        \"name\": \"ChinChai\",\r\n"
    			+ "        \"email\": \"Chai08@epam.info\",\r\n"
    			+ "        \"gender\": \"female\",\r\n"
    			+ "        \"status\": \"active\"\r\n"
    			+ "    }";
			
			Response afterUpdate=given().auth()                
					.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")                
					.header("Content-type","application/json")                
					.body(employeeData)                
					.put("https://gorest.co.in/public/v2/users/1322852");
			afterUpdate.then()
	    	.statusCode(200)
	    	.header("Content-Encoding", "gzip")
			.header("Content-type","application/json; charset=utf-8")
			.statusLine("HTTP/1.1 200 OK");		        
			afterUpdate.prettyPrint(); 
	//After Update : Verification
			System.out.println("Verification");
			Response response=given()                
					.auth()                
					.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")                
					.get("https://gorest.co.in/public/v2/users/1322852");
				response.prettyPrint();
				response.then().assertThat()
				.body("name", equalTo("ChinChai"))
				.body("email", equalTo("Chai08@epam.info"))
				.body("gender", equalTo("female"))
				.body("status", equalTo("active"))
				.statusCode(200)
				.header("Content-Encoding", "gzip")
				.header("Content-type","application/json; charset=utf-8")
				.statusLine("HTTP/1.1 200 OK");
			
			}
	@Test
    public void delete() {
    	Response response =given()                
				.when()                
				.auth()                
				.oauth2("7fefab40cc1c215bc5093de01203cdc93542788f04f2685fd2c768a19a383538")                          
				.delete("https://gorest.co.in/public/v2/users/1322852");
    	response.then().statusCode(204).statusLine("HTTP/1.1 204 No Content");
    	System.out.println(response.getStatusCode());

	}
}

