package com.epam.POSTHomeTask;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Map;
public class PetHomeTask {    
	@Test    
	public void addPet()    {        
		RestAssured.baseURI="https://petstore.swagger.io/v2";        
		RequestSpecification request = RestAssured.given();        
		String petData= """
			{
					  "id": 12345,
					  "category": {
					    "id": 1,
					    "name": "dog"
					  },
					  "name": "snoopie",
					  "photoUrls": [
					    "string"
					  ],
					  "tags": [
					    {
					      "id": 0,
					      "name": "string"
					    }
					  ],
					  "status": "pending"
					}""";
		Response response = request.header("Content-type","application/json")                
				.body(petData).post("/pet");        
		response.prettyPrint();    
		}
	@Test    
	public void getPetValidation()    {        
		RestAssured.baseURI="https://petstore.swagger.io/v2";        
		RequestSpecification request = RestAssured.given();       
		Response response = request.request(Method.GET, "/pet/12345");
		System.out.println(response.getStatusCode());
		System.out.println(response.getHeader("Content-type"));
		JSONObject jsonObject = new JSONObject(response.asPrettyString()) ;        
		String pet = jsonObject.getJSONObject("category").get("name").toString();       
		String name = jsonObject.get("name").toString();        
		String status = jsonObject.get("status").toString();        
		System.out.println("Pet : " + pet 
						+ "\nName: " + name 
						+ "\nStatus: " + status);      
	
		
	}
	}
