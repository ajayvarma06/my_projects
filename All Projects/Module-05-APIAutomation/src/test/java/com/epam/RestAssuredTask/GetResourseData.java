package com.epam.RestAssuredTask;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetResourseData {
	@Test
	public void verifyPost() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();  	
    	Response response=httpRequest.request(Method.GET,"/posts/3");
    	response.prettyPrint();
    	Assert.assertEquals(response.getStatusCode(), 200);
    	Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");

	}
	@Test
	public void verifyComments() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();  	
    	Response response=httpRequest.request(Method.GET,"/comments/20");
    	response.prettyPrint();
    	Assert.assertEquals(response.getStatusCode(), 200);
    	Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");

	}
	@Test
	public void verifyAlbums() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();  	
    	Response response=httpRequest.request(Method.GET,"/albums/32");
    	response.prettyPrint();
    	Assert.assertEquals(response.getStatusCode(), 200);
    	Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");

	}
	@Test
	public void verifyPhotos() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();  	
    	Response response=httpRequest.request(Method.GET,"/photos/23");
    	response.prettyPrint();
    	Assert.assertEquals(response.getStatusCode(), 200);
    	Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");

	}
	@Test
	public void verifyTodos() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();  	
    	Response response=httpRequest.request(Method.GET,"/todos/27");
    	response.prettyPrint();
    	Assert.assertEquals(response.getStatusCode(), 200);
    	Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");

	}
	@Test
	public void verifyUsers() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();  	
    	Response response=httpRequest.request(Method.GET,"/users/5");
    	response.prettyPrint();
    	Assert.assertEquals(response.getStatusCode(), 200);
    	Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");

	}
}
