package com.epam.RestAssuredTask;

import org.testng.annotations.Test;
import io.restassured.RestAssured;

public class DeleteResponseData {
	@Test
	public void deletePost() {
		RestAssured.given().when().delete("https://jsonplaceholder.typicode.com/posts/11")
		.then().assertThat().statusCode(200);
		
	}
	@Test
	public void deleteComments() {
		RestAssured.given().when().delete("https://jsonplaceholder.typicode.com/comments/4")
		.then().assertThat().statusCode(200);
		
	}
	@Test
	public void deleteAlbums() {
		RestAssured.given().when().delete("https://jsonplaceholder.typicode.com/albums/28")
		.then().assertThat().statusCode(200);
		
	}
	@Test
	public void deletePhotos() {
		RestAssured.given().when().delete("https://jsonplaceholder.typicode.com/photos/13")
		.then().assertThat().statusCode(200);
		
	}
	@Test
	public void deleteTodos() {
		RestAssured.given().when().delete("https://jsonplaceholder.typicode.com/todos/35")
		.then().assertThat().statusCode(200);
		
	}
	@Test
	public void deleteUsers() {
		RestAssured.given().when().delete("https://jsonplaceholder.typicode.com/users/4")
		.then().assertThat().statusCode(200);
		
	}

}
