package com.epam.RestAssuredTask;

import java.util.ArrayList;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ResponseCount 
{
    @Test
    public void validatePostsCount()
    {
    	RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();
    	Response ResponsePosts=httpRequest.request(Method.GET,"/posts");
    	int statusCode=ResponsePosts.getStatusCode();
    	if(statusCode==200){
    		ArrayList<?> listResponse=ResponsePosts.as(ArrayList.class);
        	System.out.println("Number of Posts : "+listResponse.size());
    	}
    	else {
    		System.out.println("Failed with Responce Code: "+statusCode);
    	}
    }	
    @Test
    public void validateCommentsCount() {
    	RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();
    	Response ResponseComments=httpRequest.request(Method.GET,"/comments");
    	int statusCodeComments=ResponseComments.getStatusCode();
    	if(statusCodeComments==200){
    		ArrayList<?> listResponse=ResponseComments.as(ArrayList.class);
        	System.out.println("Number of Comments : "+listResponse.size());
    	}
    	else {
    		System.out.println("Failed with Responce Code: "+statusCodeComments);
    	}}

    @Test
    public void validateAlbumsCount() {
		RestAssured.baseURI="https://jsonplaceholder.typicode.com";
    	RequestSpecification httpRequest=RestAssured.given();
    	Response ResponseAlbums=httpRequest.request(Method.GET,"/albums");
    	int statusCodeAlbums=ResponseAlbums.getStatusCode();
    	if(statusCodeAlbums==200){
    		ArrayList<?> listResponse=ResponseAlbums.as(ArrayList.class);
        	System.out.println("Number of Albums : "+listResponse.size());
    	}
    	else {
    		System.out.println("Failed with Responce Code: "+statusCodeAlbums);
    	}
    }
    
    @Test
    public void validatePhotosCount() {
    	RestAssured.baseURI="https://jsonplaceholder.typicode.com";
        RequestSpecification httpRequest=RestAssured.given();
    	Response ResponsePhotos=httpRequest.request(Method.GET,"/photos");
    	int statusCodePhotos=ResponsePhotos.getStatusCode();
    	if(statusCodePhotos==200){
    		ArrayList<?> listResponse=ResponsePhotos.as(ArrayList.class);
        	System.out.println("Number of Photos : "+listResponse.size());
    	}
    	else {
    		System.out.println("Failed with Responce Code: "+statusCodePhotos);
    	}
    }	
    @Test
    public void validateTodosCount() {
    	RestAssured.baseURI="https://jsonplaceholder.typicode.com";
        RequestSpecification httpRequest=RestAssured.given();
    	Response ResponseTodos=httpRequest.request(Method.GET,"/todos");
    	int statusCodeTodos=ResponseTodos.getStatusCode();
    	if(statusCodeTodos==200){
    		ArrayList<?> listResponse=ResponseTodos.as(ArrayList.class);
        	System.out.println("Number of Todos : "+listResponse.size());
    	}
    	else {
    		System.out.println("Failed with Responce Code: "+statusCodeTodos);
    	}
    }
    
    @Test
    public void validateUsersCount() {
    	RestAssured.baseURI="https://jsonplaceholder.typicode.com";
        RequestSpecification httpRequest=RestAssured.given();
    	Response ResponseUsers=httpRequest.request(Method.GET,"/users");
    	int statusCodeUsers=ResponseUsers.getStatusCode();
    	if(statusCodeUsers==200){
    		ArrayList<?> listResponse=ResponseUsers.as(ArrayList.class);
        	System.out.println("Number of Users : "+listResponse.size());
    	}
    	else {
    		System.out.println("Failed with Responce Code: "+statusCodeUsers);
    	}
    }
    
}

