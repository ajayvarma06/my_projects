package com.epam.RestAssuredTask;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.*;
import static io.restassured.RestAssured.given;
import static java.lang.Thread.sleep;

public class PutResourseData {
	@Test
	public void modifyPost() { 
		String bodyString="{\r\n"
				+ "    \"id\": 1,\r\n"
				+ "    \"title\": \"Validate post\",\r\n"
				+ "    \"body\": \"usto sed quo iuresvolup\",\r\n"
				+ "    \"userId\": 4\r\n"
				+ "}";
		Response response=given()
    	.when()
		.body(bodyString)
		.contentType("application/json")
		.put("https://jsonplaceholder.typicode.com/posts/1");
		response.then()
		.assertThat()
		.statusCode(200)
		.body("userId", equalTo(4))
		.body("title", equalTo("Validate post"))
		.body("body", equalTo("usto sed quo iuresvolup")).extract();
		response.prettyPrint();
			
	}
	@Test
	public void modifyComments() {
		String bodyString="{\r\n"
				+ "    \"name\": \"Validate comments\",\r\n"
				+ "    \"postId\": 4,\r\n"
				+ "    \"id\": 31,\r\n"
				+ "    \"body\": \"usto gsed qgsuo iuresvolup\",\r\n"
				+ "    \"email\": \"asbfh@cfsf.org\"\r\n"
				+ "}";
		Response response=given()
		    	.when()
				.body(bodyString)
				.contentType("application/json")
				.put("https://jsonplaceholder.typicode.com/comments/31");
		response.then()
		.assertThat()
		.statusCode(200)
		.body("postId", equalTo(4))
		.body("name", equalTo("Validate comments"))
		.body("email", equalTo("asbfh@cfsf.org"))
		.body("body", equalTo("usto gsed qgsuo iuresvolup")).extract();
		response.prettyPrint();

	}
	@Test
	public void modifyAlbums() {
		String bodyString="{\r\n"
				+ "    \"id\": 31,\r\n"
				+ "    \"title\": \"Validate albums\",\r\n"
				+ "    \"userId\": 4\r\n"
				+ "}";
		Response response=given()
		    	.when()
				.body(bodyString)
				.contentType("application/json")
				.put("https://jsonplaceholder.typicode.com/albums/31");
		response.then()
		.assertThat()
		.statusCode(200)
		.body("userId", equalTo(4))
		.body("title", equalTo("Validate albums")).extract();
		response.prettyPrint();

	}
	@Test
	public void modifyPhotos() {
		String bodyString="{\r\n"
				+ "    \"albumId\": 4,\r\n"
				+ "    \"id\": 27,\r\n"
				+ "    \"title\": \"Validate Photos\",\r\n"
				+ "    \"url\": \"https://via.placeholder.com/600/e9\",\r\n"
				+ "    \"thumbnailUrl\": \"https://via.placeholder.com/150/e92\"\r\n"
				+ "}";
		Response response=given()
		    	.when()
				.body(bodyString)
				.contentType("application/json")
				.put("https://jsonplaceholder.typicode.com/photos/27");
		response.then()
		.assertThat()
		.statusCode(200)
		.body("albumId", equalTo(4))
		.body("title", equalTo("Validate Photos"))
		.body("url", equalTo("https://via.placeholder.com/600/e9"))
		.body("thumbnailUrl", equalTo("https://via.placeholder.com/150/e92")).extract();
		response.prettyPrint();

	}
	@Test
	public void modifyTodos() { 
		String bodyString="{\r\n"
				+ "    \"id\": 27,\r\n"
				+ "    \"completed\": \"true\",\r\n"
				+ "    \"title\": \"Validate todos\",\r\n"
				+ "    \"userId\": 4\r\n"
				+ "}";
		Response response=given()
		    	.when()
				.body(bodyString)
				.contentType("application/json")
				.put("https://jsonplaceholder.typicode.com/todos/27");
		response.then()
		.assertThat()
		.statusCode(200)
		.body("userId", equalTo(4))
		.body("title", equalTo("Validate todos"))
		.body("completed", equalTo("true")).extract();
		response.prettyPrint();

	}
	@Test
	public void modifyUsers() {
		JSONObject bodyJsonObject=new JSONObject();
		bodyJsonObject.put("name", "abc");
		bodyJsonObject.put("id", 5);
		bodyJsonObject.put("username", "qwertyu");
		bodyJsonObject.put("email", "Hettinger@annie.c"); 
		bodyJsonObject.put("address", "{\r\n"
				+ "        \"street\": \"Skiles Walks\",\r\n"
				+ "        \"suite\": \"Suite 351\",\r\n"
				+ "        \"city\": \"Roscoeview\",\r\n"
				+ "        \"zipcode\": \"33263\",\r\n"
				+ "        \"geo\": {\r\n"
				+ "            \"lat\": \"-31.8129\",\r\n"
				+ "            \"lng\": \"62.5342\"\r\n"
				+ "        }");
		bodyJsonObject.put("phone", "876543456");
		bodyJsonObject.put("website", "abc.info");
		bodyJsonObject.put("company", "{\r\n"
				+ "        \"name\": \"Keebler LLC\",\r\n"
				+ "        \"catchPhrase\": \"User-centric fault-tolerant solution\",\r\n"
				+ "        \"bs\": \"revolutionize end-to-end systems\"\r\n"
				+ "    }");
		Response response=given()
		    	.when()
				.body(bodyJsonObject.toString())
				.contentType("application/json")
				.put("https://jsonplaceholder.typicode.com/users/10");
		response.then()
		.assertThat()
		.statusCode(200)
		.body("name", equalTo("abc"))
		.body("username", equalTo("qwertyu"))
		.body("email", equalTo("Hettinger@annie.c"))
		.body("address", equalTo("{\r\n"
				+ "        \"street\": \"Skiles Walks\",\r\n"
				+ "        \"suite\": \"Suite 351\",\r\n"
				+ "        \"city\": \"Roscoeview\",\r\n"
				+ "        \"zipcode\": \"33263\",\r\n"
				+ "        \"geo\": {\r\n"
				+ "            \"lat\": \"-31.8129\",\r\n"
				+ "            \"lng\": \"62.5342\"\r\n"
				+ "        }"))
		.body("phone", equalTo("876543456"))
		.body("website", equalTo("abc.info"))
		.body("company", equalTo("{\r\n"
				+ "        \"name\": \"Keebler LLC\",\r\n"
				+ "        \"catchPhrase\": \"User-centric fault-tolerant solution\",\r\n"
				+ "        \"bs\": \"revolutionize end-to-end systems\"\r\n"
				+ "    }")).extract();
		response.prettyPrint();

	}

}
