package com.epam.DataDrivenTesting;

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.annotations.Test;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;
public class WeatherAPITest {    
	@Test    
	void WeatherHyderabad()    {        
		RestAssured.baseURI = "http://api.openweathermap.org";        
		RequestSpecification httpRequest = RestAssured.given();        
		String key = "235e14b790c46e93169142c60b8bcaee";        
		Response response= httpRequest.queryParam("q", "hyderabad")
                .queryParam("appid", key)
                .request(Method.GET, "/data/2.5/weather");        
		response.prettyPrint();   
		response.then().statusCode(200);	
	}
	@Test
	public void verifyResponse(){
		RestAssured.baseURI = "http://api.openweathermap.org"; 
		String key = "235e14b790c46e93169142c60b8bcaee";
		RequestSpecification httpRequest = RestAssured.given().auth().oauth2(key);
		Response response = httpRequest.queryParam("q", "hyderabad")
		                              .queryParam("appid", key)
		                              .request(Method.GET, "/data/2.5/weather");
		//JSONObject jsonObject = new JSONObject(response.asString());
		//String longitude = jsonObject.getJSONObject("coord").get("lon").toString();
		//String latitude = jsonObject.getJSONObject("coord").get("lat").toString();
		String longitude =response.jsonPath().getString("coord.lon");
		String latitude =response.jsonPath().getString("coord.lat");
		System.out.println(longitude);
		System.out.println(latitude);
		response = httpRequest.queryParam("lat", latitude)
		                     .queryParam("lon", longitude)
		                     .queryParam("appid", key)
		                     .request(Method.GET, "/data/2.5/weather");
		response.then()
		        .body("name", equalTo("Hyderabad"))
		        .body("sys.country", equalTo("IN"))
		        .body("main.temp_min", greaterThan(0F))
		        .body("main.temp", greaterThan(0F));
	}
}
