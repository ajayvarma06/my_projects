package Java8_Task;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Functional_Interfaces_1 {
	public static void main(String[] args) {
		ProductStore productStore=new ProductStore();
		List<Product> productList =productStore.getProducts();
		System.out.println(checkPriceAbove1000(productList));
		System.out.println(checkProductCategory(productList));
		System.out.println(checkPriceAndCategory(productList));
		System.out.println(checkPriceOrCategory(productList));
		System.out.println(checkPrice100AndCategory(productList));
	}

// 1. Define a predicate to check if the price of given product is greater than 1000/-.
//    Print all the products from the given list of the products if the price is greater than 1000/-.	
////		productList.stream().filter(priceAbove1000).forEach(t->System.out.println(t));
	 public static List<Product> checkPriceAbove1000(List<Product> productList) {
		 Predicate<Product> predicate=p->p.getPrice()>1000;
		 List<Product> list=new ArrayList<>();
		 for(Product product:productList) {
			 if(predicate.test(product)) {
				 list.add(product);
			 }
		 }
		 return list;
	}


// 2. Define a predicate to check if the product is of electronics category. 
//    Print all the products from the given list of the products if the product is of electronics category. 
////		productList.stream().filter(electronics).forEach(System.out::println);
	public static List<Product> checkProductCategory(List<Product> productList) {
		Predicate<Product> predicate=p->p.getCategory().equals("Electronics");
		 List<Product> list=new ArrayList<>();
		 for(Product product:productList) {
			 if(predicate.test(product)) {
				 list.add(product);
			 }
		 }
		 return list;	
	}

		
// 3. Print all the products from the given list of product 
//    if the product price is greater than 100/- which are in electronics category.
////		productList.stream().filter(priceAbove100.and(electronics)).forEach(System.out::println);
	public static List<Product> checkPriceAndCategory(List<Product> productList) {
		Predicate<Product> predicate=p->p.getCategory().equals("Electronics") && p.getPrice()>100;
		 List<Product> list=new ArrayList<>();
		 for(Product product:productList) {
			 if(predicate.test(product)) {
				 list.add(product);
			 }
		 }
		 return list;	
	}

// 4. Print all the products from the given list of product 
//	  if the product price is greater than 100/- or product is in electronics category.
////		productList.stream().filter(priceAbove100.or(electronics)).forEach(System.out::println);
	public static List<Product> checkPriceOrCategory(List<Product> productList) {
		Predicate<Product> predicate=p->p.getCategory().equals("Electronics") || p.getPrice()>100;
		 List<Product> list=new ArrayList<>();
		 for(Product product:productList) {
			 if(predicate.test(product)) {
				 list.add(product);
			 }
		 }
		 return list;	
	}
		
		
// 5. Print all the products from the given list of product 
//	  if the product price is less than 100/- and product is in electronics category.		
////		productList.stream().filter(product->product.getCategory().equals("Electronics") && product.getPrice()<100).forEach(System.out::println);
	public static List<Product> checkPrice100AndCategory(List<Product> productList) {
		Predicate<Product> predicate=p->p.getCategory().equals("Electronics") && p.getPrice()<100;
		 List<Product> list=new ArrayList<>();
		 for(Product product:productList) {
			 if(predicate.test(product)) {
				 list.add(product);
			 }
		 }
		 return list;	
	}
}





