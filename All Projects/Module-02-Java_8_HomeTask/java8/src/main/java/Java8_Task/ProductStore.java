package Java8_Task;

import java.util.Arrays;
import java.util.List;

public class ProductStore {
	public List<Product> getProducts(){
        Product product1 = new Product("Electronics", "Iphone", 480, "Bronze");
        Product product2 = new Product("Electronics", "MacBook", 15, "Gold");
        Product product3 = new Product("Electronics", "Sony Video Projector", 420, "Silver");       
        Product product4 = new Product("Women's Fashion", "Fast-Track Watch", 99, "Bronze");
        Product product5 = new Product("Women's Fashion", "Leather HandBag", 2500, "Platinum");
        Product product6 = new Product("Men's Fashion", "T-Shirt", 500, "Platinum");
        Product product7 = new Product("Men's Fashion", "Reebok Shoes", 50, "Silver");
        Product product8 = new Product("Men's Fashion", "Titan Watch", 4800, "Gold");
        Product product9 = new Product("Kids", "HeadBands", 200, "Platinum");
        Product product10 = new Product("Kids", "Canvas Cap", 1200, "Bronze");
        Product product11 = new Product("Kids", "Baby SunGlasses", 42, "Silver");
        Product product12 = new Product("Kids", "iLearn Baby Toys", 190, "Gold");
        Product product13 = new Product("Kids", "Montessori Toys", 89, "Silver");
        Product product14 = new Product("Books", "Clean Code Principles", 1100, "Bronze");
        Product product15 = new Product("Books", "Microservices", 230, "Platinum");
        Product product16 = new Product("Books", "Design Patterns & Principles", 850, "Silver");
        Product product17 = new Product("Kids", "Indoor Playground Set", 4300, "Silver");
        Product product18 = new Product("Electronics", "Andriod Phone", 999, "Bronze");

        return Arrays.asList(product1, product2, product3, product4, product5, product6, product7, product8,
                product9, product10, product11, product12, product13, product14, product15, product16,product17,product18);


}}
