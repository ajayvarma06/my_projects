package Java8_Task;

public class Product {
	private String category;
	private String itemName;
    private double price;
    private String grade;
    
    public Product(String itemName,double price) {
    	this.itemName=itemName;
    	this.price=price;
    }
    
    
	public Product(String category, String itemName, double price, String grade) {
		super();
		this.category = category;
		this.itemName = itemName;
		this.price = price;
		this.grade = grade;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	@Override
	public String toString() {
		return "Product [category=" + category + 
				", itemName=" + itemName + 
				", price=" + price + 
				", grade=" + grade + "]";
	}


}
