package Java8_Task;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Functional_Interface_3 {
	public static void main(String[] args) {
		ProductStore productStore=new ProductStore();
		List<Product> productList =productStore.getProducts();
			
		System.out.println(findCostAllProducts(productList));
		System.out.println(findCostAbove1000(productList));
		System.out.println(findCostElectronicProducts(productList));
		System.out.println(findCost1000AndElectronic(productList));
	}
//	1. Write a function to calculate the cost of all products in a given list of products. 
////		System.out.println(productList.stream().mapToDouble(Product::getPrice).sum());
	public static double findCostAllProducts(List<Product> products) {
		Function<Product, Double> function=p->p.getPrice();
		double price=0.0;
		for(Product product:products) {
			price +=function.apply(product);
		}
		return price;
		}	
		
//	2. Write a function to calculate the cost of all products whose prices is > 1000/- in the given list of products.
////		System.out.println(productList.stream().filter(priceAbove1000).mapToDouble(Product::getPrice).sum());
	public static double findCostAbove1000(List<Product> products) {
		Function<Product, Double> function=p->p.getPrice();
		Predicate<Product> predicate=p->p.getPrice()>1000;
		double price=0.0;
		for(Product product:products) {
			if(predicate.test(product)) {
				price +=function.apply(product);
			}
		}
		return price;
		}
		
//	3. Write a function to calculate the cost of all electronic products in the given list of products. 
////		System.out.println(productList.stream().filter(electronics).mapToDouble(Product::getPrice).sum());
	public static double findCostElectronicProducts(List<Product> products) {
		Function<Product, Double> function=p->p.getPrice();
		Predicate<Product> predicate=p->p.getCategory().equals("Electronics");
		double price=0;
		for(Product product:products) {
			if(predicate.test(product)) {
				price +=function.apply(product);
			}
		}
		return price;
		}
		
//	4. Write a function to get all the products whose price is is > 1000/- and belongs to electronic category.
////		System.out.println(productList.stream().filter(priceAbove1000.and(electronics)).mapToDouble(Product::getPrice).sum());
	public static double findCost1000AndElectronic(List<Product> products) {
		Function<Product, Double> function=p->p.getPrice();
		Predicate<Product> predicate=p->p.getPrice()>1000 && p.getCategory().equals("Electronics");
		double price=0;
		for(Product product:products) {
			if(predicate.test(product)) {
				price +=function.apply(product);
			}
		}
		return price;
		}
}
 