package Java8_Task;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;


public class Bi_Function {
	public static void main(String[] args) {

//	1. Given the name and price of the product,write a Bifunction to create a product. 
		BiFunction<String, Integer, Product> createProduct = 
				(name,price)-> new Product(name, price);
			List<Product> list= new ArrayList<>();
			list.add(createProduct.apply("Keypad", 800));
			list.add(createProduct.apply("HeadSet", 1200));
			list.add(createProduct.apply("Printer", 3000));
			System.out.println(list);
		
//	2. Given the Product and quantity of the products,write a BiFunction to calculate the cost of products. 
//	A cart is a map of product and quantity.Given the cart, calculate the cost of the cart. 	
			
		BiFunction<Product, Integer, Double> productCost=(product , quantity)-> product.getPrice()*quantity;
		Map<Product, Integer> productMap=new HashMap<>();
		productMap.put(new Product("Keypad", 800),15);
		productMap.put(new Product("HeadSet", 1200),5);
		productMap.put(new Product("Printer", 3000),2);
		int cost=0;
		for (Map.Entry<Product, Integer> it: productMap.entrySet()) {
			cost += productCost.apply(it.getKey(), it.getValue());
		}
		System.out.println("Cost of the cart : "+cost);
		}

	
}
