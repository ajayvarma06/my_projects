package Java8_Task;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Functional_Interface_2 {
	public static void main(String[] args) {
		ProductStore productStore=new ProductStore();
		List<Product> productList =productStore.getProducts();
		
		consumerLogToFile(productList);
		updateGrade(productList);
		updateProductNameSuffix(productList);
		printPremiumProductsuffix(productList);
		randomProduct(productList);
		randomOTP(productList);
	}
	
// CONSUMER TASK
	
// 1. Given a product write a consumer to print the product to appropriate medium depending on the print parameter. 
//	If the print parameter is set to file, consumer shall log the product to file, if not print on the console. 
	
	public static void consumerLogToFile(List<Product> productList){   
		Consumer<String> consumer = input -> {       
			if(input.equals("file")) {          
				File file = new File("Consumer.txt");               
				try {                   
					FileWriter fileWriter = new FileWriter(file);                    
					for (Product product : productList) {                      
						if (file.createNewFile()) {                           
							System.out.println("File Created : " + file.getName());                
							} 
						else {                        
							if (file.setWritable(true)) {                       
								fileWriter.append(product.toString()+"\n");               
								}                
							}             
						}                  
					fileWriter.close();             
					} 
				catch (IOException e){ throw new RuntimeException(e); }           
				System.out.println("All Products added to the file."); 
				System.out.println("-----------------------------------------");
				}
			else{           
				for(Product product : productList){          
					System.out.println(product.toString());    
					}      
				}  
		};    
			consumer.accept("file");}
	
//	2. Write a Consumer to update the grade of the product as 'Premium' if the price is > 1000/-. 
//	Given the product list, update the grade for each product and print all of the products. 	
////		productList.stream().filter(priceAbove1000).forEach(setPremium.andThen(System.out::println));
		public static void updateGrade(List<Product> productList) {
			Consumer<Product> consumer=p->{
				if(p.getPrice()>1000) {
					p.setGrade("Premium");
				}};
				for(Product product:productList) {
					consumer.accept(product);
					System.out.println(product);
				}
				System.out.println("-----------------------------------------");
		}
		
//	3. Write a Consumer to update the name of the product to be suffixed with '*' if the price of product is > 3000/-. 
//		Given the product list, update the name for each product and print all of the products. 
////		productList.stream().filter(priceAbove3000).forEach(suffixName.andThen(System.out::println));
		public static void updateProductNameSuffix(List<Product> productList) {
			Consumer<Product> consumer=p->{
				if(p.getPrice()>3000) {
					p.setItemName(p.getItemName()+"*");
				}};
			for (Product product:productList) {
				consumer.accept(product);
				System.out.println(product);
			}
			System.out.println("------------------------------------------");
		}
		
//	4. Print all the Premium grade products with name suffixed with '*'.
////		productList.stream().filter(Premium.and(nameWithSuffix)).forEach(System.out::println);
		public static void printPremiumProductsuffix(List<Product> productList) {
			Consumer<Product> consumer=p->{
				if(p.getGrade().equals("Premium") && p.getItemName().endsWith("*")) {
					System.out.println(p+"*");
				}};
				for(Product product:productList) {
					consumer.accept(product);
				}
				System.out.println("----------------------------------------");
		}
		
// SUPPLIER TASK
		
//		1. Write a supplier to produce a random product. 
		public static void randomProduct(List<Product> productList) {
		Supplier<Product> sup=()->{
			Product randomProduct=productList.get(0);
			randomProduct=productList.get((int)((Math.random()*10)));
			return randomProduct;
		};
		System.out.println(sup.get());
		System.out.println("----------------------------------------");
		}
		
//		2. Write a supplier to produce a random OTP. 
		public static void randomOTP(List<Product> productList) {
		Supplier<String> supplier=()->{
			String otpString="";
			for(int i=0;i<6;i++) {otpString=otpString+(int)(Math.random()*10);}
			return otpString;
		};
		System.out.println(supplier.get());
		}
		}
