package Java8_Task;
import java.util.Scanner;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;

public class Binary_Unary_Operator {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
//		1. Write an IntPredicate to verify if the given number is a primenumber
		System.out.println("Give a number : ");
		int a=sc.nextInt();
		IntPredicate prime=num->{
			if(num<=1) {return false;}
			for(int i=2;i<num;i++) {
				if(num%i==0) {return false;}
			}
			return true;};
		if(prime.test(a)) {System.out.println("Number is palindrome");}
		else {System.out.println("Number is Not a palindrome");}
		
//		2. Write an IntConsumer to print square of the given number
		System.out.println("To find Square,Give a number : ");
		int b=sc.nextInt();
		IntConsumer printSquare=num->System.out.println("Square of the Number is : "+num*num);
		printSquare.accept(b);
		
//		3. Write a IntSupplier to give random int below 5000. 
		IntSupplier ranIntSupplier=()->(int)(Math.random()*5000);
		System.out.println("Random Int below 5000 is : "+ranIntSupplier.getAsInt());
	}
}
