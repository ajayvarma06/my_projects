package Constructor_Reference_Task;

class Employee {
    String Name;
    String Account;
    double Salary;
    public Employee (String name, String  account, double salary) {
        this.Name = name;
        this.Salary = salary;
        this.Account = account;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "Name='" + Name + '\'' +
                ", Account='" + Account + '\'' +
                ", Salary=" + Salary +
                '}';
  } 
    }