package Constructor_Reference_Task;

@FunctionalInterface
interface EmployeeInfo {
     Employee infoEmployee(String name, String account, double salary);
}
