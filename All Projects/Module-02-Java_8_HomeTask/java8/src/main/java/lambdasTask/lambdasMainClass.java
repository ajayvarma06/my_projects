package lambdasTask;
import java.util.*;


public class lambdasMainClass {
	public static void main(String[] args) {
		
		lambdasMethods lambdasMethods=new lambdasMethods();
		
		List<Integer> numberList =new ArrayList<>();
		numberList.add(50);
		numberList.add(5);
		numberList.add(100);
		numberList.add(20);
		numberList.add(1);
		
		List<Employee> employees =Arrays.asList(
				new Employee("Ajay",30),
				new Employee("Nobita",20),
				new Employee("Priya",15));
		
		//Method Calls
		
		lambdasMethods.checkPalindrome("madam");
		lambdasMethods.secondBiggestNumber(numberList);
		lambdasMethods.checkStringRotations("ABCD", "DABC");
		lambdasMethods.startNewThread();
		lambdasMethods.sortNumbersInReverse(numberList);
		lambdasMethods.sortEmployeeList(employees);
		lambdasMethods.sortNumbersInReverseTreeset();
		lambdasMethods.sortEmployeeTreeset();
		lambdasMethods.sortNumbersInReverseTreeMap();
		lambdasMethods.sortEmployeesInReverseTreeMap();
		lambdasMethods.sortEmployeesInReverse(employees);
	
		
	}

}
	
