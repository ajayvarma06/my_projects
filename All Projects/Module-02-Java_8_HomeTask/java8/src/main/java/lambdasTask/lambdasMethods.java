package lambdasTask;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class lambdasMethods {

//	1. Check if a given string is a palindrome
	public  void checkPalindrome(String str) {
		Predicate<String> palindromeLogic=s->{
			int left=0;
			int right=s.length()-1;
			while (left<right) {
				if(s.charAt(left++)!=s.charAt(right--)) {return false;}	
			}
			return true;
		};
		Consumer<String> checkConsumer =s->{
		if (palindromeLogic.test(str)==true) {
			System.out.printf("The String '%s' is Palindrome\n",str);}
		else {System.out.printf("The String '%s' is Palindrome\n",str);}};
		checkConsumer.accept(str);
		}
	
//	2. Find the 2nd biggest number in the given list of numbers
	public  void secondBiggestNumber(List<Integer> numberList) {
	    numberList.sort((a,b)->b-a);
	    System.out.println(numberList.get(1));
		}

//	3. write a program to check if two strings are rotations of each other. 
	public  void checkStringRotations(String str1,String str2) {
		BiPredicate<String, String> areRotations =(s1,s2)->(s1.length()==s2.length() && (s1+s2).contains(s2));
		if (areRotations.test(str1, str2)==true) {
			System.out.println("String 1 and String 2 are Rotations");}
		else {System.out.println("String 1 and String 2 are  Not Rotations");}
		}

//	4. Use Runnable interface to start a new thread and print numbers from 
	public  void startNewThread() {
		Runnable runnable=()-> {
			for(int i=1;i<=10;i++) {System.out.println(i);}};
		new Thread(runnable).start();
		}

//	5. Use Comparator interface to sort given list of numbers in reverse order
	public  void sortNumbersInReverse(List<Integer> numberList) {
		Collections.sort(numberList,(num1,num2)->num2-num1);
		System.out.println(numberList);
	}
	
//	6. Use Comparator interface to sort given list of Employees in the alphabetic order of their name
	public  void sortEmployeeList(List<Employee> employees) {
		Collections.sort(employees,(str1,str2)->str1.getName().compareTo(str2.getName()));
		System.out.println(employees);
	}
	
//	7. Create a TreeSet that sorts the given set of numbers in reverse order
	public  void sortNumbersInReverseTreeset() {
		Set<Integer> numbers =new TreeSet<>((o1,o2)->o2-o1);
		numbers.addAll(Arrays.asList(50,2,-5,20,5));
		System.out.println(numbers);
	}
	
//	8. Create a TreeSet that sorts the given set of Employees in the alphabetic order of their name
	public void sortEmployeeTreeset() {
		TreeSet<Employee> lst = new TreeSet<>((o1,o2)->o1.getName().compareTo(o2.getName()));
			lst.add(new Employee("Ajay",30));
			lst.add(new Employee("Nobita",20));
			lst.add(new Employee("Priya",15));
		System.out.println(lst);
	}
	
//	9. Create a TreeMap that sorts the given set of values in descending order
	public  void sortNumbersInReverseTreeMap() {
		TreeMap<Integer, String> map =new TreeMap<>((s1,s2)->s2.compareTo(s1));
		map.put(100, "Ajay");
		map.put(10, "Nobita");
		map.put(2, "Bhargav");
		map.put(1, "YoYo");
		map.put(20, "Priya");
		System.out.println(map);
	}
	
//	10. Create a TreeMap that sorts the given set of employees in descending order of their name
	public  void sortEmployeesInReverseTreeMap() {
		TreeMap<Integer, Employee> treeMap=new TreeMap<>((s1,s2)->s2.compareTo(s1));
		treeMap.put(1, new Employee("Ajay", 30));
		treeMap.put(1, new Employee("Priya", 25));
		treeMap.put(1, new Employee("Bhavani", 80));
		System.out.println(treeMap);
	}
	
//	11. Use Collections.Sort to sort the given list of Employees in descending order of their name		
	public  void sortEmployeesInReverse(List<Employee> employees) {
		Collections.sort(employees,(str1,str2)->str2.getName().compareTo(str1.getName()));
		System.out.println(employees);		
	}

}