package StreamsAPI_Task;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamAPI {

//	1.	How many male and female employees are there in the organization?
	public  void male_and_female(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.groupingBy(Employee::getGender,Collectors.counting())));
		System.out.println("---------------------------------------------");
	}
	
//	2.	Print the name of all departments in the organization?
	public  void name_dept(List<Employee> employees) {
		 employees.stream()
		 .map(Employee::getDepartment)
		 .distinct()
		 .forEach(System.out::println);
		 System.out.println("---------------------------------------------");
	}
	
//	3.	What is the average age of male and female employees?
	public  void avg_age(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.groupingBy(Employee::getGender,Collectors.averagingInt(Employee::getAge))));
		System.out.println("---------------------------------------------");
	}
	
//	4.	Get the details of highest paid employee in the organization?
	public  void highest_paid_Emp(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.maxBy(Comparator.comparingDouble(Employee::getSalary))).get());
		System.out.println("---------------------------------------------");
	}
	
//	5.	Get the names of all employees who have joined after 2015?
	public  void emp_joinedAfter2015(List<Employee> employees) {
		employees.stream()
		.filter(p->p.getYearOfJoining()>2015)
		.map(Employee::getName)
		.forEach(System.out::println);
		System.out.println("---------------------------------------------");
	}
	
//	6.	Count the number of employees in each department?
	public  void numberOf_emp_inDept(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment,Collectors.counting())));
		System.out.println("---------------------------------------------");
	}
	
//	7.	What is the average salary of each department?
	public  void avg_sal(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment,Collectors.averagingDouble(Employee::getSalary))));
		System.out.println("---------------------------------------------");
	}
	
//	8.	Get the details of youngest male employee in the product development department?
	public  void youngest_male_emp(List<Employee> employees) {
		Object object=  employees.stream()
				.filter(p->p.getGender().equals("Male") && p.getDepartment().equals("Product Development"))
		                 .collect(Collectors.minBy(Comparator.comparingInt(Employee::getAge))).get();
							//.min(Comparator.comparingInt(Employee::getAge));
		System.out.println(object);
		System.out.println("---------------------------------------------");
	}
	
//	9.	Who has the most working experience in the organization?
	public  void highest_work_exp(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.minBy(Comparator.comparingDouble(Employee::getYearOfJoining))).get());
				//.sorted(Comparator.comparingInt(Employee::getYearOfJoining)).findFirst().get());
		System.out.println("---------------------------------------------");
	}
	
//	10.	How many male and female employees are there in the sales and marketing team?
	public  void numberOf_maleFemale_in_SM(List<Employee> employees) {
		System.out.println(employees.stream()
				.filter(p->p.getDepartment().equals("Sales And Marketing"))
				.collect(Collectors.groupingBy(Employee::getGender,Collectors.counting())));
		System.out.println("---------------------------------------------");
	}
	
//	11.	What is the average salary of male and female employees?
	public  void avg_sal_maleFemale(List<Employee> employees) {
		System.out.println(employees.stream()
				.collect(Collectors.groupingBy(Employee::getGender,Collectors.averagingDouble(Employee::getSalary))));
		System.out.println("---------------------------------------------");
	}
	
//	12.	List down the names of all employees in each department?
	public void all_Emp(List<Employee> employees) {
	  Map<String, List<String>> object = employees.stream()
		        .collect(Collectors.groupingBy(Employee::getDepartment,
		        		Collectors.mapping(Employee::getName, Collectors.toList())));
		System.out.println(object);
		System.out.println("---------------------------------------------");
	}
	
//	13.	What is the average salary and total salary of the whole organization?
	public void avgAndTotal_sal(List<Employee> employees) {
		DoubleSummaryStatistics object = employees.stream()
				.collect(Collectors.summarizingDouble(Employee::getSalary));
	    System.out.println("Average Salary = "+object.getAverage());
	    System.out.println("Total Salary = "+object.getSum());
		System.out.println("---------------------------------------------");
	}
	
//	14.	Separate the employees who are younger or equal to 25 years from those employees who are older than 25 years.
	public void emp_sep(List<Employee> employees) {
		System.out.println("Employees Younger or equal to 25 years:... ");
		employees.stream().filter(p->p.getAge()<=25).map(Employee::getName).forEach(System.out::println);
		System.out.println();
		System.out.println("Employees above 25 years:... ");
		employees.stream().filter(p->p.getAge()>25).map(Employee::getName).forEach(System.out::println);
		//System.out.println(employees.stream().collect(Collectors.partitioningBy(e -> e.getAge() > 25)));
		System.out.println("---------------------------------------------");
	}
	
//	15.	Who is the oldest employee in the organization? What is his age and which department he belongs to?
	public void oldestEmp(List<Employee> employees) {
		System.out.println("Oldest Employee in the Organization: ");
		System.out.println(employees.stream()
				.max(Comparator.comparingInt(Employee::getAge))
				.map(e->e.getName()+", Age :"+ e.getAge() +", Department :"+e.getDepartment()).get());				
	}
}
