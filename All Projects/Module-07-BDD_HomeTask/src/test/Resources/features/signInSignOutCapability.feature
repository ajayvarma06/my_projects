Feature: SignIn and SignOut

  Background: launch the browser
    Given I Navigated on LogIn page from HomePage

  Scenario Outline: Test signIn Functionality with Correct Credentials
    Given Iam on the SignIn Page
    When I enter Email:"<Email>"
    And I enter Password:"<Password>"
    And I click on SignIn button
    Then If login is Successful click Logout or DeleteAccount

    Examples: 
      | Email              | Password |
      | ajayepam@gmail.com | password |

  Scenario Outline: Test signIn Functionality with Wrong Credentials
    Given Iam on the SignIn Page
    When I enter Email:"<Email>"
    And I enter Password:"<Password>"
    And I click on SignIn button
    Then It should show Error Message

    Examples: 
      | Email            | Password |
      | ajay123@shop.com | password |

  Scenario: Test signIn Functionality without any Credentials
    Given Iam on the SignIn Page
    When I click on SignIn button
    Then Email field Shows message Please fill out this field

  Scenario Outline: Test signIn Functionality with  Email without Password
    Given Iam on the SignIn Page
    When I enter Email:"<Email>"
    And I click on SignIn button
    Then Password field Shows message Please fill out this field

    Examples: 
      | Email         |
      | ajay@shop.com |

  Scenario Outline: Test signIn Functionality With Invalid Email Format
    Given Iam on the SignIn Page
    When I enter Email:"<Email>"
    And I enter Password:"<Password>"
    And I click on SignIn button
    Then It should show message to enter @ or values after @ message

    Examples: 
      | Email | Password |
      | ajay@ | password |
