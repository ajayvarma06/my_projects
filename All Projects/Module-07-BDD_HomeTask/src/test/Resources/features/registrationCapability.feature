Feature: Registration on the online Shopping portal

  Background: launch the browser
    Given I Navigated on SignUp page from HomePage

  Scenario: Try to SignUp without Name and Email
    Given I am on the SignUp page
    And click on SignUp button
    Then Name field  Shows message Please fill out this field

  Scenario: Try to SignUp with Name but without Email
    Given I am on the SignUp page
    When I enter Name in Respective Field
      | name | ajay varma |
    And click on SignUp button
    Then Email field  Shows message Please fill out this field

  Scenario: Try to SignUp with invalid email
    Given I am on the SignUp page
    When I Enter Name and Invalid Email in Respective Fields
      | name  | ajay varma       |
      | email | ajay123gmail.com |
    And click on SignUp button
    Then It should show message to enter @ or values after @

  Scenario: Registration with existing email
    Given I am on the SignUp page
    When I Enter Name and Existing Email in Respective Fields
      | name  | ajay varma            |
      | email | ajay1234@shopping.com |
    And click on SignUp button
    Then I should see an error message for existing email

  Scenario: New User SignUp functionality fill only required fields
    Given I am on the SignUp page
    When I Enter Name and Email in Respective Fields
      | name  | ajay varma         |
      | email | ajayepam@gmail.com |
    And click on SignUp button
    Then I should be successfully navigate to registration Page
    When I Enter Required Details for Registration
      | title        | Mr         |
      | password     | password   |
      | firstName    | ajay       |
      | lastName     | varma      |
      | address      | Chintal    |
      | country      | India      |
      | state        | Telangana  |
      | city         | Hyderabad  |
      | zipcode      |     512345 |
      | mobileNumber | 9876543219 |
    And I Click On CreateAccount button
    Then The Account is created Successfully and click on Continue
    And Iam on the Shopping HomePage
    Then I should click Logout or DeleteAccount

  Scenario: New User SignUp functionality fill all fields
    Given I am on the SignUp page
    When I Enter Name and Email in Respective Fields
      | name  | ajay varma           |
      | email | ajayepamer@gmail.com |
    And click on SignUp button
    Then I should be successfully navigate to registration Page
    When I Enter All fields Details for Registration
      | title         | mr         |
      | password      | password   |
      | day           |          1 |
      | month         | January    |
      | year          |       2020 |
      | newsletter    | true       |
      | specialOffers | true       |
      | firstName     | ajay       |
      | lastName      | varma      |
      | company       | company    |
      | address       | Chintal    |
      | address2      | RajNagar   |
      | country       | India      |
      | state         | Telangana  |
      | city          | Hyderabad  |
      | zipcode       |     512345 |
      | mobileNumber  | 9876543219 |
    And I Click On CreateAccount button
    Then The Account is created Successfully and click on Continue
    And Iam on the Shopping HomePage
    Then I should click Logout or DeleteAccount
