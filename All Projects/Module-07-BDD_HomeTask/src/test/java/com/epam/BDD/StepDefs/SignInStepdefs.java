package com.epam.BDD.StepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.IOException;
import org.testng.Assert;

import com.epam.BDD.pages.HomePage;

public class SignInStepdefs {
	PicoContainer picoContainer;

	public SignInStepdefs(PicoContainer picoContainer) throws IOException {
		this.picoContainer = picoContainer;
		this.picoContainer.setDriver();
	}

	@Given("I Navigated on LogIn page from HomePage")
	public void iNavigatedOnSignUpPageFromHomePage() {
		picoContainer.setHomePage(new HomePage(picoContainer.getDriver()));
		picoContainer.getHomePage().launchHomePage();
	}

	@Given("Iam on the SignIn Page")
	public void iamOnTheSignInPage() {
		picoContainer.setSignUpAndLoginPage(picoContainer.getHomePage().clickOnSignUpOrLogin());
	}

	@When("I click on SignIn button")
	public void iClickOnSignInButton() {
		picoContainer.getSignUpAndLoginPage().clickSignIn();
	}

	@When("I enter Email:{string}")
	public void iEnterEmail(String email) {
		picoContainer.getSignUpAndLoginPage().enterEmailForSignIn(email);
	}

	@And("I enter Password:{string}")
	public void iEnterPassword(String password) {
		picoContainer.getSignUpAndLoginPage().enterPasswordForSignIn(password);
	}

	@Then("If login is Successful click Logout or DeleteAccount")
	public void ifLoginIsSuccessfulClickLogoutOrDeleteAccount() {
		System.out.println(picoContainer.getDriver().getCurrentUrl());
		picoContainer.getHomePage().logOutfromShoppingPage();
		Assert.assertEquals(picoContainer.getDriver().getCurrentUrl(), "https://automationexercise.com/login");

	}

	@Then("It should show Error Message")
	public void itShouldShowErrorMessage() {
		System.out.println(picoContainer.getSignUpAndLoginPage().errorMessage());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().errorMessage(),
				"Your email or password is incorrect!");
	}

	@Then("Email field Shows message Please fill out this field")
	public void emailFieldShowsMessagePleaseFillOutThisField() {
		System.out.println(picoContainer.getSignUpAndLoginPage().emailMessage());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().emailMessage(), "Please fill out this field.");

	}

	@Then("Password field Shows message Please fill out this field")
	public void passwordFieldShowsMessagePleaseFillOutThisField() {
		System.out.println(picoContainer.getSignUpAndLoginPage().passwordMessage());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().passwordMessage(), "Please fill out this field.");

	}

	@Then("It should show message to enter @ or values after @ message")
	public void itShouldShowMessageToEnterOrValuesAfterMessage() {
		System.out.println(picoContainer.getSignUpAndLoginPage().emailMessage());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().emailMessage(), "Please enter an email address.");
	}
}
