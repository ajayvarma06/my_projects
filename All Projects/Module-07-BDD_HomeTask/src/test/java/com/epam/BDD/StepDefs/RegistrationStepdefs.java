package com.epam.BDD.StepDefs;

import io.cucumber.datatable.DataTable;
import java.io.IOException;
import java.util.Map;
import org.testng.Assert;

import com.epam.BDD.pages.HomePage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegistrationStepdefs {
	PicoContainer picoContainer;

	public RegistrationStepdefs(PicoContainer picoContainer) throws IOException {
		this.picoContainer = picoContainer;
		this.picoContainer.setDriver();
	}

	@Given("I Navigated on SignUp page from HomePage")
	public void iNavigatedOnSignUpPageFromHomePage() {
		picoContainer.setHomePage(new HomePage(picoContainer.getDriver()));
		picoContainer.getHomePage().launchHomePage();
	}

	@Given("I am on the SignUp page")
	public void iAmOnTheSignUpPage() {
		picoContainer.setSignUpAndLoginPage(picoContainer.getHomePage().clickOnSignUpOrLogin());
	}

	@And("click on SignUp button")
	public void clickOnSignUpButton() {
		picoContainer.setRegistrationPage(picoContainer.getSignUpAndLoginPage().navigateToRegistrationPage());
	}

	@Then("I should be successfully navigate to registration Page")
	public void iShouldBeSuccessfullyNavigateToRegistrationPage() {
		System.out.println(picoContainer.getDriver().getCurrentUrl());
		Assert.assertEquals(picoContainer.getDriver().getCurrentUrl(), "https://automationexercise.com/signup");
	}

	@And("I Click On CreateAccount button")
	public void iClickOnCreateAccountButton() {
		picoContainer.getRegistrationPage().clickCreateAccount();
	}

	@Then("The Account is created Successfully and click on Continue")
	public void theAccountIsCreatedSuccessfullyAndClickOnContinue() {
		picoContainer.getRegistrationPage().continueStep();
		System.out.println(picoContainer.getDriver().getCurrentUrl());
		//Assert.assertEquals(picoContainer.getDriver().getCurrentUrl(), "https://automationexercise.com/");

	}

	@Then("I should see an error message for existing email")
	public void iShouldSeeAnErrorMessageForExistingEmail() {
		System.out.println(picoContainer.getSignUpAndLoginPage().errorEmailExist());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().errorEmailExist(), "Email Address already exist!");

	}

	@Then("Name field  Shows message Please fill out this field")
	public void nameFieldShowsMessagePleaseFillOutThisField() {
		System.out.println(picoContainer.getSignUpAndLoginPage().nameMessageString());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().nameMessageString(), "Please fill out this field.");
	}

	@Then("Email field  Shows message Please fill out this field")
	public void emailFieldShowsMessagePleaseFillOutThisField() {
		System.out.println(picoContainer.getSignUpAndLoginPage().emailMessageString());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().emailMessageString(), "Please fill out this field.");

	}

	@Then("It should show message to enter @ or values after @")
	public void itShouldShowMessageToEnterOrValuesAfter() {
		System.out.println(picoContainer.getSignUpAndLoginPage().emailMessageString());
		Assert.assertEquals(picoContainer.getSignUpAndLoginPage().emailMessageString(),
				"Please enter an email address.");
	}

	@When("I Enter Name and Email in Respective Fields")
	public void iEnterNameAndEmailInRespectiveFields(DataTable dataTable) {
		Map<String, String> requiredDetails = dataTable.asMap(String.class, String.class);
		picoContainer.getSignUpAndLoginPage().enterEmail(requiredDetails.get("email"));
		picoContainer.getSignUpAndLoginPage().enterName(requiredDetails.get("name"));
	}

	@When("I Enter Required Details for Registration")
	public void iEnterRequiredDetailsForRegistration(DataTable dataTable) {
		Map<String, String> requiredDetails = dataTable.asMap(String.class, String.class);
		picoContainer.getRegistrationPage().enterFirstName(requiredDetails.get("firstName"));
		picoContainer.getRegistrationPage().enterLastName(requiredDetails.get("lastName"));
		picoContainer.getRegistrationPage().enterPassword(requiredDetails.get("password"));
		picoContainer.getRegistrationPage().enterAddress(requiredDetails.get("address"));
		picoContainer.getRegistrationPage().entercity(requiredDetails.get("city"));
		picoContainer.getRegistrationPage().enterState(requiredDetails.get("state"));
		picoContainer.getRegistrationPage().enterZipcode(requiredDetails.get("zipcode"));
		picoContainer.getRegistrationPage().enterMobileNumber(requiredDetails.get("mobileNumber"));
		picoContainer.getRegistrationPage().chooseCountry(requiredDetails.get("country"));
	}

	@When("I Enter Name and Existing Email in Respective Fields")
	public void iEnterNameAndExistingEmailInRespectiveFields(DataTable dataTable) {
		Map<String, String> requiredDetails = dataTable.asMap(String.class, String.class);
		picoContainer.getSignUpAndLoginPage().enterEmail(requiredDetails.get("email"));
		picoContainer.getSignUpAndLoginPage().enterName(requiredDetails.get("name"));
	}

	@When("I enter Name in Respective Field")
	public void iEnterNameInRespectiveField(DataTable dataTable) {
		Map<String, String> requiredDetails = dataTable.asMap(String.class, String.class);
		picoContainer.getSignUpAndLoginPage().enterName(requiredDetails.get("name"));
	}

	@When("I Enter Name and Invalid Email in Respective Fields")
	public void iEnterNameAndInvalidEmailInRespectiveFields(DataTable dataTable) {
		Map<String, String> requiredDetails = dataTable.asMap(String.class, String.class);
		picoContainer.getSignUpAndLoginPage().enterEmail(requiredDetails.get("email"));
		picoContainer.getSignUpAndLoginPage().enterName(requiredDetails.get("name"));
	}

	@And("Iam on the Shopping HomePage")
	public void iamOnTheShoppingHomePage() {
		System.out.println(picoContainer.getDriver().getCurrentUrl());
	}

	@Then("I should click Logout or DeleteAccount")
	public void iShouldClickLogoutOrDeleteAccount() {
		//picoContainer.getHomePage().logOutfromShoppingPage();
		picoContainer.getHomePage().clickDeleteAccount();
	}

    @When("I Enter All fields Details for Registration")
    public void iEnterAllFieldsDetailsForRegistration(DataTable dataTable) {
		Map<String, String> requiredDetails = dataTable.asMap(String.class, String.class);
		picoContainer.getRegistrationPage().chooseTitle(requiredDetails.get("title"));
		picoContainer.getRegistrationPage().enterFirstName(requiredDetails.get("firstName"));
		picoContainer.getRegistrationPage().enterLastName(requiredDetails.get("lastName"));
		picoContainer.getRegistrationPage().enterPassword(requiredDetails.get("password"));
		picoContainer.getRegistrationPage().chooseDay(requiredDetails.get("day"));
		picoContainer.getRegistrationPage().chooseMonth(requiredDetails.get("month"));
		picoContainer.getRegistrationPage().chooseYear(requiredDetails.get("year"));
		picoContainer.getRegistrationPage().enterAddress(requiredDetails.get("address"));
		picoContainer.getRegistrationPage().enterAddress1(requiredDetails.get("address2"));
		picoContainer.getRegistrationPage().entercity(requiredDetails.get("city"));
		picoContainer.getRegistrationPage().enterState(requiredDetails.get("state"));
		picoContainer.getRegistrationPage().enterZipcode(requiredDetails.get("zipcode"));
		picoContainer.getRegistrationPage().enterMobileNumber(requiredDetails.get("mobileNumber"));
		picoContainer.getRegistrationPage().chooseCountry(requiredDetails.get("country"));
		picoContainer.getRegistrationPage().optInNewsLetter(Boolean.valueOf(requiredDetails.get("newsletter")));
		picoContainer.getRegistrationPage().getSpecialOffers(Boolean.valueOf(requiredDetails.get("specialOffers")));

    }
}
