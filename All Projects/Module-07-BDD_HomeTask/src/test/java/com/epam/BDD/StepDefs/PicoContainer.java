package com.epam.BDD.StepDefs;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.epam.BDD.factory.DriverManagerFactory;
import com.epam.BDD.pages.HomePage;
import com.epam.BDD.pages.RegistrationPage;
import com.epam.BDD.pages.SignUpAndLoginPage;

import io.cucumber.java.After;

public class PicoContainer {
	 private WebDriver driver;
	HomePage homePage;
	RegistrationPage registrationPage;
	SignUpAndLoginPage signUpAndLoginPage;
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver() throws IOException {
		if(this.driver==null)
		this.driver = DriverManagerFactory.getTheDriver().getDriver();
	}
	public HomePage getHomePage() {
		return homePage;
	}
	public void setHomePage(HomePage homePage) {
		if(this.homePage==null)
		this.homePage = homePage;
	}
	public RegistrationPage getRegistrationPage() {
		return registrationPage;
	}
	public void setRegistrationPage(RegistrationPage registrationPage) {
		if(this.registrationPage==null)
		this.registrationPage = registrationPage;
	}
	public SignUpAndLoginPage getSignUpAndLoginPage() {
		return signUpAndLoginPage;
	}
	public void setSignUpAndLoginPage(SignUpAndLoginPage signUpAndLoginPage) {
		if(this.signUpAndLoginPage==null)
		this.signUpAndLoginPage = signUpAndLoginPage;
	}
	@After
	public void tearDown() {
		driver.close();
	}
}
