package com.epam.BDD.pages;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignUpAndLoginPage {
	protected WebDriver driver;
	public SignUpAndLoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(xpath ="//input[@data-qa='signup-name']" )
	private WebElement signUpName;
	@FindBy(xpath = "//input[@data-qa='signup-email']")
	private WebElement signUpEmail;
	@FindBy(xpath = "//button[@data-qa='signup-button']")
	private WebElement signUpButton;
	@FindBy(xpath = "//form[@action='/signup']//p")
	private WebElement emailAlreadyExistError;
	
	public String nameMessageString() {
		new WebDriverWait(driver, Duration.ofSeconds(3));
		return signUpName.getAttribute("validationMessage");
	}
	public String emailMessageString() {
		new WebDriverWait(driver, Duration.ofSeconds(3));
		return signUpEmail.getAttribute("validationMessage");
	}
	
	public RegistrationPage navigateToRegistrationPage() {
		signUpButton.click();
		return new RegistrationPage(driver);
	}
	
	public void enterName(String name) {
		signUpName.sendKeys(name);
	}
	
	public void enterEmail(String email) {
		signUpEmail.sendKeys(email);
	}
	
	public String errorEmailExist() {

		return emailAlreadyExistError.getText();
	}
	
	@FindBy(xpath ="//input[@data-qa='login-email']")
	private WebElement signInEmail;
	@FindBy(xpath = "//input[@data-qa='login-password']")
	private WebElement signInPassword;
	@FindBy(xpath = "//button[@data-qa='login-button']")
	private WebElement loginButton;
	@FindBy(xpath = "//div[@class='login-form']//p")
	private WebElement errorMessagElement;
	
	
	public String passwordMessage() {
		new WebDriverWait(driver, Duration.ofSeconds(3));
		return signInPassword.getAttribute("validationMessage");
	}
	public String emailMessage() {
		new WebDriverWait(driver, Duration.ofSeconds(3));
		return signInEmail.getAttribute("validationMessage");
	}
		
	public void enterEmailForSignIn(String email) {
		signInEmail.clear();
		signInEmail.sendKeys(email);
		
	}
	
	public void enterPasswordForSignIn(String password) {
		signInPassword.clear();
		signInPassword.sendKeys(password);
	}
	
	public HomePage clickSignIn() {
		loginButton.click();
		return new HomePage(driver);
	}
	
	
	public String errorMessage() {
		return errorMessagElement.getText();
	}
	
}
