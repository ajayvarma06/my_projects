package com.epam.BDD.pages;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.epam.BDD.utility.Constants;

public class RegistrationPage {
	protected WebDriver driver;
	public void getRegistrationPage(){
		driver.get(Constants.SIGNUP_PAGE_URL);
	}
	public RegistrationPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "id_gender1")
	private WebElement titleMr;
	@FindBy(id = "id_gender2")
	private WebElement titleMrs;
	@FindBy(id = "days")
	private WebElement day;
	@FindBy(id = "months")
	private WebElement months;
	@FindBy(id = "years")
	private WebElement years;
	@FindBy(id = "password")
	private WebElement loginpassword;
	@FindBy(id = "first_name")
	private WebElement firstName;
	@FindBy(id = "last_name")
	private WebElement lastName;
	@FindBy(id = "address1")
	private WebElement addressElement;
	@FindBy(id = "address2")
	private WebElement addressElement1;
	@FindBy(id = "company")
	private WebElement company;
	@FindBy(id = "newsletter")
	private WebElement newsletter;
	@FindBy(id = "optin")
	private WebElement uniformOption;
	@FindBy(id = "state")
	private WebElement stateElement;
	@FindBy(id = "country")
	private WebElement countryElement;
	@FindBy(id = "city")
	private WebElement cityElement;
	@FindBy(id = "zipcode")
	private WebElement zipcodeElement;
	@FindBy(id = "mobile_number")
	private WebElement number;
	@FindBy(xpath = "//button[@data-qa='create-account']")
	private WebElement createAccountButton;
	@FindBy(partialLinkText = "Continue")
	private WebElement continueButton;

	public void chooseTitle(String title) {
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOf(titleMr));
		if (title.equalsIgnoreCase("mr")){titleMr.click();}
		else if(title.equalsIgnoreCase("mrs")){titleMrs.click();}
	}
	public void enterPassword(String password) {
		loginpassword.click();
		loginpassword.sendKeys(password);
	}
	public void enterFirstName(String first_Name) {
		firstName.click();
		firstName.sendKeys(first_Name);
	}
	public void enterLastName(String last_Name) {
		lastName.click();
		lastName.sendKeys(last_Name);
	}


	public void enterAddress(String address) {
		addressElement.click();
		addressElement.sendKeys(address);
	}
	public void enterState(String state) {
		stateElement.click();
		stateElement.sendKeys(state);
	}
	
	public void chooseCountry(String country) {
		Select select= new Select(countryElement);
		select.selectByVisibleText(country);
	}

	public void entercity(String city) {
		cityElement.click();
		cityElement.sendKeys(city);
	}
	public void enterZipcode(String zipCode) {
		zipcodeElement.click();
		zipcodeElement.sendKeys(zipCode);
	}
	public void enterMobileNumber(String mobileNumber) {
		number.click();
		number.sendKeys(mobileNumber);
	}
	
	public void clickCreateAccount() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
		wait.until(ExpectedConditions.elementToBeClickable(createAccountButton));
		createAccountButton.click();
	}
	
	public void continueStep() {
		if(!driver.getCurrentUrl().equals(Constants.ACCOUNT_CREATED_PAGE_URL)) {
		driver.navigate().refresh();
		}
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
		wait.until(ExpectedConditions.visibilityOf(continueButton));
		continueButton.click();
		driver.navigate().refresh();
	}

	public void enterAddress1(String address2) {
		addressElement1.click();
		addressElement1.sendKeys(address2);
	}

	public void chooseDay(String dayNumber) {
		Select select= new Select(day);
		select.selectByVisibleText(dayNumber);
	}

	public void chooseMonth(String month) {
		Select select= new Select(months);
		select.selectByVisibleText(month);
	}

	public void chooseYear(String year) {
		Select select= new Select(years);
		select.selectByVisibleText(year);
	}
	public void optInNewsLetter(Boolean choice){
		if(Boolean.TRUE.equals(choice)!=newsletter.isSelected())
			newsletter.click();
	}

	public void getSpecialOffers(Boolean choice){
		if(Boolean.TRUE.equals(choice)!=uniformOption.isSelected())
			uniformOption.click();
	}



}
