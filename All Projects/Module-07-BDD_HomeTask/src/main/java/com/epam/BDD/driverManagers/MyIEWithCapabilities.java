package com.epam.BDD.driverManagers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class MyIEWithCapabilities extends DriverManager{

	public MyIEWithCapabilities() {
		EdgeOptions options = new EdgeOptions();
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        driver= new EdgeDriver(options);

	}

}
