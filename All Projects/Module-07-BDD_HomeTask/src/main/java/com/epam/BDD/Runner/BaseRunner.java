package com.epam.BDD.Runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
glue = "com.epam.BDD.pages",
features = {"src/test/Resources/features/"})
public class BaseRunner extends AbstractTestNGCucumberTests {
	
}