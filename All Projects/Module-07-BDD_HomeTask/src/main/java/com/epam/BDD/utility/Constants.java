package com.epam.BDD.utility;

public class Constants {

	public final static String BASE_URL="https://automationexercise.com/";
	public final static String SIGNUP_PAGE_URL="https://automationexercise.com/signup";
	public final static String LOGIN_SIGNUP_PAGE_URL="https://automationexercise.com/login";
	public final static String ACCOUNT_CREATED_PAGE_URL="https://automationexercise.com/account_created";
	public final static String ACCOUNT_DELETED_PAGE_URL="https://automationexercise.com/delete_account";

}
