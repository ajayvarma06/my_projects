package com.epam.BDD.factory;

import java.io.IOException;
import com.epam.BDD.driverManagers.DriverManager;
import com.epam.BDD.driverManagers.MyChromeDriver;
import com.epam.BDD.driverManagers.MyChromeDriverWithCapabilities;
import com.epam.BDD.driverManagers.MyFirefoxDriver;
import com.epam.BDD.driverManagers.MyFirefoxWithCapabilities;
import com.epam.BDD.driverManagers.MyIEDriver;
import com.epam.BDD.driverManagers.MyIEWithCapabilities;
import com.epam.BDD.utility.FileReader;

public class DriverManagerFactory {

	public static DriverManager getTheDriver() throws IOException {
		DriverManager driverManager=null;
		switch (FileReader.readPropertiesFile("src/main/resources/browser.properties").getProperty("browser")) {
		case "CHROME":
			driverManager=new MyChromeDriver();
			break;
		case "FIREFOX":
			driverManager=new MyFirefoxDriver();
			break;
		case "EDGE":
			driverManager= new MyIEDriver();
			break;
		case "CHROMEWITHCAPABILITIES":
			driverManager=new MyChromeDriverWithCapabilities();
			break;
		case "FIREFOXWITHCAPABILITIES":
			driverManager=new MyFirefoxWithCapabilities();
			break;
		case "EDGEWITHCAPABILITIES":
			driverManager=new MyIEWithCapabilities();
			break;
		default:
			System.out.println("Invalid Browser name");
			break;
		}
		return driverManager;
	}

}
