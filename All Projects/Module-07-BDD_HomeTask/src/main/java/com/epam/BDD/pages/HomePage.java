package com.epam.BDD.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.epam.BDD.utility.Constants;
 

public class HomePage {
	protected WebDriver driver;
	public HomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "a[href='/login']")
	private WebElement loginPageButton;
	public SignUpAndLoginPage clickOnSignUpOrLogin() {
		loginPageButton.click();
		return new SignUpAndLoginPage(driver);
	}

	public void launchHomePage() {
		driver.get(Constants.BASE_URL);
		driver.manage().window().maximize();
	}
	
	@FindBy(css = "a[href='/logout']")
	private WebElement logout;
	public void logOutfromShoppingPage() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.elementToBeClickable(logout));
		logout.click();
	}
	
	private By deleteAccount=By.xpath("//div[@class='shop-menu pull-right']/ul/li[5]");
	public void clickDeleteAccount() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
		wait.until(ExpectedConditions.elementToBeClickable(deleteAccount));
		driver.findElement(deleteAccount).click();
	}
	
}
