package com.epam.TestNgTask;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listener implements ITestListener
{

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println(result.getName()+" Method Passed");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println(result.getName()+" Method Failed");
	}

}
