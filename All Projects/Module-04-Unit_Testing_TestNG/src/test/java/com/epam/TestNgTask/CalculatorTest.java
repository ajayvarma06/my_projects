package com.epam.TestNgTask;

import org.testng.annotations.*;
import org.testng.*;
import com.epam.tat.module4.*;
public class CalculatorTest extends AnnotationsTests
{
	Calculator calculator=new Calculator();
	
	@Test(groups="positive",dataProviderClass = DataProviderClass.class,dataProvider = "positiveTests")
	public void testAddition(long a,long b){
		long expected=a+b;
		long actual=calculator.sum(a, b);
		Assert.assertEquals(expected, actual);
		}
	
	@Test(groups="positive",dataProviderClass = DataProviderClass.class,dataProvider = "positiveTests")
	public void testSubtraction(long a,long b){
		long expected=a-b;
		long actual=calculator.sub(a, b);
		Assert.assertEquals(expected, actual);
		}
	
	@Test(groups="positive",dataProviderClass = DataProviderClass.class,dataProvider = "positiveTests")
	public void testMultiplication(long a,long b){
		long expected=a*b;
		long actual=calculator.mult(a, b);
		Assert.assertEquals(expected, actual);
		}
	
	@Test(groups="positive",dataProviderClass = DataProviderClass.class,dataProvider = "positiveTests")
	public void testDivison(long a,long b){
		long expected=a/b;
		long actual=calculator.div(a, b);
		Assert.assertEquals(expected, actual);
		}
	@Test(groups="positive",dataProviderClass = DataProviderClass.class,dataProvider = "positiveSingleData")
	public void testSqrt(double number){
		Double expected=Math.sqrt(number);
		Double actual=calculator.sqrt(number);
		Assert.assertEquals(expected,actual);
		}

//Negatives Tests
	
	@Test(groups = "negative")
    void testSumInvalid()
    {
        System.out.println("inside negative testSum");
        long result=calculator.sum(3, 2);
        Assert.assertEquals(result,6,"Sum test failed");
    }
    @Test(groups = "negative")
    void testSubtractionInvalid()
    {
        System.out.println("inside negative testSum");
        long result=calculator.sub(2, 3);
        Assert.assertEquals(result, 5, "Sub test failed");
    }
    @Test(groups = "negative")
    void testMultiplicationInvalid()
    {
        System.out.println("inside negative testMul");
        long result=calculator.mult(2, 3);
        Assert.assertEquals(result,9 ,"Mult test failed");
    }
    @Test(groups = "negative")
    void testTanInvalid()
    {
        System.out.println("inside negative testTan");
        double result=calculator.tg(90.0);
        Assert.assertEquals(result,4,"Tan test failed");

    }
    @Test(groups = "negative")
    void testSqrtInvalid()
    {
        System.out.println("inside negative testSqrt");
        double result=calculator.sqrt(9.0);
        Assert.assertEquals(result,3.1," sqrt test failed");
    }
    
//Finding Bugs
	@Test(groups = "findingBugs",expectedExceptions = ArithmeticException.class,
			dataProviderClass = DataProviderClass.class,dataProvider = "divisionByZero")
    void testDivisionForBugs(int x,int y)
    {
		System.out.println("findBug in Div");
		String expected= "/ by zero";
        Assert.assertEquals(calculator.div(x,y),expected);
    }
    @Test(groups = "findingBugs",dataProviderClass = DataProviderClass.class,dataProvider = "doubleMultiplication")
    void testMultiplicationForBugs(Double x,Double y,Double expected)
    { 
        System.out.println("findBug in mult");
        Assert.assertEquals(calculator.mult(x, y),expected);
    }
    @Test(groups = "findingBugs",dataProviderClass = DataProviderClass.class,dataProvider = "doublePower")
    void testDoublePowerForBugs(Double x,Double y,Double expected)
    {
        System.out.println("findBug in Power");
        Assert.assertEquals(calculator.pow(x,y),expected);
    }
    @Test(groups = "findingBugs",dataProviderClass = DataProviderClass.class,dataProvider = "doubleSqrt")
    void testDoubleSqrtForBugs(Double x,Double expected)
    {
        System.out.println("findBug  inside Sqrt");
        Assert.assertEquals(calculator.sqrt(x),expected);
    }
    @Test(groups = "findingBugs",dataProviderClass = DataProviderClass.class,dataProvider = "doubletg")
    void testDoubleTgForBugs(Double x,Double expected)
    {
        System.out.println("findBug in  TG");
        Assert.assertEquals(calculator.tg(x),expected);
    }
    @Test(groups = "findingBugs",dataProviderClass = DataProviderClass.class, dataProvider = "doublecos")
    void testDoubleCosForBugs(Double x,Double expected)
    {
        System.out.println("findBug in Cosine");
        Assert.assertEquals(calculator.cos(x),expected);
    }
}

