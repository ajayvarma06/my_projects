package com.epam.TestNgTask;

import org.testng.annotations.*;

public class DataProviderClass {
	@DataProvider(name="positiveTests")
	public Object[][] positiveTests()
	{
		return new Object[][]
				{
					{3,2},
					{4,8},
					{2,5},
					{-29,-40},
					{1,2},
					{45,-75}
			   };
			
	}

	@DataProvider(name="positiveSingleData")
	public Object[] positiveSingleData()
	{
		return new Object[]
				{
				30,2,55,3,7,90,45
				};
	}
	
	@DataProvider(name = "divisionByZero")
    public Object[][] denominatorZero()
    {
        return new Object[][]{
                {2,0},
                {5,0},
                {10,0}
        };
    }
	
    @DataProvider(name = "doubleMultiplication")
    public Object[][] doubleMultiplication()
    {
        return new Object[][]{
                {2.0,6.4,2.0*6.4},
                {7.5,4.2,7.5*4.2},
                {6.5,9.2,6.5*9.2}
        };
    }
    @DataProvider(name = "doublePower")
    public Object[][] doublePower()
    {
        return new Object[][]{
                {2.0,3.0,Math.pow(2.0,3.0)},
                {7.5,3.8,Math.pow(7.5,3.8)},
                {9.2,6.5,Math.pow(9.2,6.5)}
        };
    }
    @DataProvider(name = "doubleSqrt")
    public Object[][] doubleSqrt()
    {
        return new Object[][]{
                {4.0,Math.sqrt(4.0)},
                {-4.0,Math.sqrt(-4.0)},
                {-9.0,Math.sqrt(-9.0)},
                {5.2,Math.sqrt(5.2)},
                {3.0,Math.sqrt(3.0)}
        };
    }
    @DataProvider(name = "doubletg")
    public Object[][] doubletg()
    {
        return new Object[][]{
                {4.0,Math.tan(4.0)},
                {-4.0,Math.tan(-4.0)},
                {0.0,Math.tan(0.0)},
                {90.0,Math.tan(90.0)}
        };
    }
    
    @DataProvider(name = "doublecos")
    public Object[][] doublecos()
    {
        return new Object[][]{
                {3.0,Math.cos(3.0)},
                {-4.0,Math.cos(-4.0)},
                {90.0,Math.cos(90.0)},
                {0.0,Math.cos(0.0)}
        };
    }
    @DataProvider(name = "doublesin")
    public Object[][] doublesin()
    {
        return new Object[][]{
                {3.0,Math.sin(3.0)},
                {-4.0,Math.sin(-4.0)},
                {-90.0,Math.sin(-90.0)},
                {0.0,Math.sin(0.0)}
        };
    }
}
