package com.epam.TestNgTask;
import com.epam.tat.module4.*;
import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class CustomRunner {

	public static void main(String[] args) {
		TestNG testNG=new TestNG();
		XmlSuite suite = new XmlSuite();
		suite.setName("Calculator Functionality");
		XmlTest test = new XmlTest(suite);
		test.setName("TmpTest");
		List<XmlClass> classes = new ArrayList<XmlClass>();
		classes.add(new XmlClass("com.epam.TestNgTask.CalculatorTest"));
		test.setXmlClasses(classes) ;
//		List<String> files=new ArrayList<>();
//		files.addAll(new ArrayList<String>(){
//				{add("testng.xml");}});
//		suite.setSuiteFiles(files);
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		testNG.setXmlSuites(suites);
		testNG.run();

	}

}
