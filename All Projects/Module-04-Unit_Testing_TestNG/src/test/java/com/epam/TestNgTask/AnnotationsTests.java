package com.epam.TestNgTask;

import org.testng.annotations.*;

public class AnnotationsTests {
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() {
		System.out.println("Inside beforeSuite");
	}
	@BeforeGroups(alwaysRun = true)
	public void beforeGroups() {
		System.out.println("Inside beforeGroups");
	}
	
	@BeforeTest(alwaysRun = true)
	public void beforeTest() {
		System.out.println("Inside beforeTest");
	}
	
	@BeforeClass(alwaysRun = true)
	public void beforeClass() {
		System.out.println("Inside beforeClass");
	}
	@BeforeMethod(alwaysRun = true)
	public void beforeMethod() {
		System.out.println("Inside beforeMethod");
	}
	
	@AfterSuite(alwaysRun = true)
	public void afterSuite() {
		System.out.println("Inside afterSuite");
	}
	@AfterGroups(alwaysRun = true)
	public void afterGroups() {
		System.out.println("Inside AfterGroups");
	}
	
	@AfterTest(alwaysRun = true)
	public void afterTest() {
		System.out.println("Inside afterTest");
	}
	@AfterClass(alwaysRun = true)
	public void afterClass() {
		System.out.println("Inside afterClass");
	}
	@AfterMethod(alwaysRun = true)
	public void afterMethod() {
		System.out.println("Inside afterMethod");
	}
}
