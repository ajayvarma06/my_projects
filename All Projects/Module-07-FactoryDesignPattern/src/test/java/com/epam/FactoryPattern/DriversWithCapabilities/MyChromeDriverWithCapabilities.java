package com.epam.FactoryPattern.DriversWithCapabilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyChromeDriverWithCapabilities implements DriverManagerWithCapabilities{

	@Override
	public WebDriver getDriverWithCapabilities() {
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        return new ChromeDriver(options);


	}
	
}
