package com.epam.FactoryPattern.DriversWithCapabilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class MyIEWithCapabilities implements DriverManagerWithCapabilities{

	@Override
	public WebDriver getDriverWithCapabilities() {
		EdgeOptions options = new EdgeOptions();
        options.addArguments("--headless");
        return new EdgeDriver(options);

	}

}
