package com.epam.FactoryPattern.DriversWithCapabilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class MyFirefoxWithCapabilities implements DriverManagerWithCapabilities {

	@Override
	public WebDriver getDriverWithCapabilities() {
		FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        return new FirefoxDriver(options);

	}

}
