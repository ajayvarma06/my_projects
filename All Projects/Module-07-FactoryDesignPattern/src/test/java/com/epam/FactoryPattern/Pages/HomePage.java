package com.epam.FactoryPattern.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.epam.FactoryPattern.Utilities.DriverUtility;

public class HomePage {
	
	private WebDriver driver;
	
	public HomePage(String browserName) {
		this.driver = DriverUtility.getTheDriver(browserName);
		PageFactory.initElements(driver, this);
	}
	
	public void sitelaunch() {
		driver.get("https://www.selenium.dev/");
	}

	@FindBy(xpath = "//div[@id='main_navbar']/ul/li[3]//span")
	private WebElement clickDownloads;
	public void clickOnDownloads() {
		clickDownloads.click();
		
	}
	public void siteClose() {
		driver.quit();
	}
}
