package com.epam.FactoryPattern.Drivers;

import org.openqa.selenium.WebDriver;

public interface DriverManager {
	
	WebDriver getDriver();

}
