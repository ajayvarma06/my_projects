package com.epam.FactoryPattern.Test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.epam.FactoryPattern.Pages.HomePage;

public class HomePageTest {
	
	public HomePage homePage;
	
	@BeforeTest
	public void setUp() {
		homePage=new HomePage("Chrome With Capabilities");
	}
	
	@Test
	public void testDownloadsPage() {
		homePage.sitelaunch();
		homePage.clickOnDownloads();
	}
	@AfterTest
	public void afterTest() {
		homePage.siteClose();
	}
}
