package com.epam.FactoryPattern.Drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirefoxDriver implements DriverManager {
	public WebDriver getDriver() {
		return new FirefoxDriver();
	}
}
