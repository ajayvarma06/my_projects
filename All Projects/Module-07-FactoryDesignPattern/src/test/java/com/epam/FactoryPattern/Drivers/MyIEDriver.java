package com.epam.FactoryPattern.Drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class MyIEDriver implements DriverManager{
	public WebDriver getDriver() {
		return new InternetExplorerDriver();
	}

}
