package com.epam.FactoryPattern.Utilities;

import org.openqa.selenium.WebDriver;

import com.epam.FactoryPattern.Drivers.DriverManager;
import com.epam.FactoryPattern.Drivers.MyChromeDriver;
import com.epam.FactoryPattern.Drivers.MyFirefoxDriver;
import com.epam.FactoryPattern.Drivers.MyIEDriver;
import com.epam.FactoryPattern.DriversWithCapabilities.DriverManagerWithCapabilities;
import com.epam.FactoryPattern.DriversWithCapabilities.MyChromeDriverWithCapabilities;
import com.epam.FactoryPattern.DriversWithCapabilities.MyFirefoxWithCapabilities;
import com.epam.FactoryPattern.DriversWithCapabilities.MyIEWithCapabilities;

public class DriverUtility {
	public static WebDriver driver;
	public static WebDriver getTheDriver(String browserName) {
//		DriverManager driver=null;
//		DriverManagerWithCapabilities driverWithCapabilities=null;
		switch (browserName) {
		case "chrome":
			DriverManager chrome = new MyChromeDriver();
			driver= chrome.getDriver();
			break;
		case "firefox":
			DriverManager firefox = new MyFirefoxDriver();
			driver=firefox.getDriver();
			break;
		case "ie":
			DriverManager ie = new MyIEDriver();
			driver=ie.getDriver();
			break;
		case "Chrome With Capabilities":
			DriverManagerWithCapabilities chromeDriver = new MyChromeDriverWithCapabilities();
			driver=chromeDriver.getDriverWithCapabilities();
			break;
		case "firefox With Capabilities":
			DriverManagerWithCapabilities firefoxdriver = new MyFirefoxWithCapabilities();
			driver=firefoxdriver.getDriverWithCapabilities();
			break;
		case "ie With Capabilities":
			DriverManagerWithCapabilities iedriver = new MyIEWithCapabilities();
			driver=iedriver.getDriverWithCapabilities();
			break;
		default:
			System.out.println("Invalid Browser name");
			break;
		}
		return driver;
	}
}
