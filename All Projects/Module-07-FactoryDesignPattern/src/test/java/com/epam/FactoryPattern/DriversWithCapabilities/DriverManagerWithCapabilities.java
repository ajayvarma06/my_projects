package com.epam.FactoryPattern.DriversWithCapabilities;

import org.openqa.selenium.WebDriver;

public interface DriverManagerWithCapabilities {
	
	WebDriver getDriverWithCapabilities();
}
