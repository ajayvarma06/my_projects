package com.epam.SeleniumPOMPF.pages;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleCloudHomePage extends BasePage {
	
	public GoogleCloudHomePage(WebDriver driver) {
		super(driver);
	}
	
	public GoogleCloudHomePage getHomePage() {
		driver.get("https://cloud.google.com/");
		driver.manage().window().maximize();
		return new GoogleCloudHomePage(driver);
	}
	private By searchButton=By.xpath("//input[@class='devsite-search-field devsite-search-query']");
	public SearchResultPage searchClick() {
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(searchButton));
		WebElement searchDriver = driver.findElement(searchButton);
		searchDriver.click();
		searchDriver.sendKeys("Google Cloud Platform Pricing Calculator");
		searchDriver.submit();
		return new SearchResultPage(driver);
	}
	
}
