package com.epam.SeleniumPOMPF.tests;

import static org.testng.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.epam.SeleniumPOMPF.pages.GoogleCloudHomePage;
import com.epam.SeleniumPOMPF.pages.PricingCalculatorPage;
import com.epam.SeleniumPOMPF.pages.SearchResultPage;

public class GoogleCloudPricingTest {
	private GoogleCloudHomePage googleCloudHomePage;
	private SearchResultPage searchResultPage;
	private PricingCalculatorPage calculatorPage;
	WebDriver driver;
	
	@BeforeTest
	public void setUp() {
		FirefoxOptions options = new FirefoxOptions();
        driver= new FirefoxDriver(options);
		calculatorPage = new PricingCalculatorPage(driver);
		googleCloudHomePage = new GoogleCloudHomePage(driver);
	}
	@Test(priority = -1)
	public void testWebsite() {
		googleCloudHomePage.getHomePage();
		Assert.assertEquals(driver.getCurrentUrl(), "https://cloud.google.com/");
	}
	@Test(priority = 0)
	public void testResultPage() {
	searchResultPage =	googleCloudHomePage.searchClick();
	}
	
	@Test(priority = 1)
	public void testSearchResultFunctionality()  {
		calculatorPage = searchResultPage.navigateToCalculatorPage();
	}
	
	@Test(priority = 2)
	public void testFillingForm() {
		calculatorPage.switchFrame();
		calculatorPage.choseComputeEngine();
		calculatorPage.enterNoOfInstances();
		calculatorPage.chooseOperatingSystem();
		calculatorPage.chooseProvisioningModel();
		calculatorPage.chooseSeries();
		calculatorPage.chooseInstanceType();
		calculatorPage.selectCheckBoxGPU();
		calculatorPage.chooseGPUType();
		calculatorPage.chooseNoOfGPU();
		calculatorPage.chooseLocalSSD();
		calculatorPage.chooseLocation();
		calculatorPage.chooseCommitedUsage();
		calculatorPage.clickEstimateButton();
	}

	@Test(priority = 3)
	public void testformValuesAndCost() {
		assertTrue(calculatorPage.getRegion().contains("Region: Frankfurt"));
		assertTrue(calculatorPage.getCommitmentTerm().contains("Commitment term: 1 Year"));
		assertTrue(calculatorPage.getProvisioningModel().contains("Regular"));
		assertTrue(calculatorPage.getInstanceType().contains("n1-standard-8"));
		assertTrue(calculatorPage.getLocalSSD().contains("2x375 GiB"));
		assertTrue(calculatorPage.getEstimatedCost().contains("USD 1,081.20"));
	}
	@AfterTest
	public void tearDown() {
		driver.close();
	}


}
