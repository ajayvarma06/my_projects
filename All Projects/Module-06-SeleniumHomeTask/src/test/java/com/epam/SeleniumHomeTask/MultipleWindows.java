package com.epam.SeleniumHomeTask;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MultipleWindows {
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/windows");
		driver.manage().window().maximize();
	}
	
	@Test
	public void testMultipleWindows() {
		driver.findElement(By.partialLinkText("Click")).click();
		driver.switchTo().window(driver.getWindowHandles()
				.toArray(new String[driver.getWindowHandles().size()])[1]);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div/h3")));
		System.out.println(driver.getCurrentUrl());
		Assert.assertEquals(driver.getCurrentUrl(), "http://the-internet.herokuapp.com/windows/");
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}

}
