package com.epam.SeleniumHomeTask;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Dropdown {
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/dropdown");
		driver.manage().window().maximize();
	}
	@Test
	public void testDropdown() {
		Select select= new Select(driver.findElement(By.id("dropdown")));
		select.selectByVisibleText("Option 1");
		System.out.println(select.getFirstSelectedOption().getText()); 
//		List<WebElement> element=select.getOptions();
//	    element.forEach(webElement->element.get(2).click());
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}
}
