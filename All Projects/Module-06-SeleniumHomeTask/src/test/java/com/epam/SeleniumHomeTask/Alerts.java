package com.epam.SeleniumHomeTask;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Alerts {
	
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/javascript_alerts");
		driver.manage().window().maximize();	
	}
	
	@Test
	public void testAlerts(){

		driver.findElement(By.xpath("//button[@onclick='jsAlert()']")).click();
		Alert alert1=driver.switchTo().alert();
		System.out.println(alert1.getText());
		alert1.accept();
		
		driver.findElement(By.xpath("//button[@onclick='jsConfirm()']")).click();
		Alert alert2=driver.switchTo().alert();
		System.out.println(alert2.getText());
		alert2.dismiss();
		 
		driver.findElement(By.xpath("//button[@onclick='jsPrompt()']")).click();
		Alert alert3=driver.switchTo().alert();
		System.out.println(alert3.getText());
		alert3.sendKeys("Javascript Alert");
		alert3.accept();
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}
}
