package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DynamicContent {
	
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/dynamic_content");
		driver.manage().window().maximize();
	}
	
	@Test
	public void testDynamicContent() {
//		WebElement dynamicElement=driver.findElement(By.xpath("//div[@id='content']/descendant::div/p[2]/a"));
//		dynamicElement.click();
		
		WebElement image1=driver.findElement(By.xpath("(//div[@class='large-2 columns']/img)[1]"));
		System.out.println("Image1 Source :"+image1.getAttribute("src"));
		
		WebElement image2=driver.findElement(By.xpath("(//div[@class='large-2 columns']/img)[2]"));
		System.out.println("Image2 Source :"+image2.getAttribute("src"));
		
		WebElement image3=driver.findElement(By.xpath("(//div[@class='large-2 columns']/img)[3]"));
		System.out.println("Image3 Source :"+image3.getAttribute("src"));
		
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}
	
}
