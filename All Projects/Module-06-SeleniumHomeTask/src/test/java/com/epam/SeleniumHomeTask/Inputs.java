package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Inputs {
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/inputs");
		driver.manage().window().maximize();	
	}
	
	@Test
	public void testInputs() {
		
		WebElement input=driver.findElement(By.tagName("input"));
		input.sendKeys("10");
	}
	
	@AfterTest
	public void afterTest() {
		driver.close();
	}
}
