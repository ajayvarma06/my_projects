package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CheckBox {
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/checkboxes");
		driver.manage().window().maximize();
	}
	
	@Test
	public void testCheckBox() {
		
		WebElement checkBox1=driver.findElement(By.xpath("//form[@id='checkboxes']/input[1]"));
		//WebElement checkBox1=driver.findElement(By.cssSelector("input[type='checkbox']:nth-of-type(1)"));
		System.out.println("Check box 1 :"+checkBox1.isSelected());
		checkBox1.click();
		System.out.println("Check box 1 :"+checkBox1.isSelected());
		WebElement checkBox2=driver.findElement(By.xpath("//form[@id='checkboxes']/input[2]"));
		System.out.println("Check box 2 :"+checkBox2.isSelected());
		checkBox2.click();
		System.out.println("Check box 2 :"+checkBox2.isSelected());
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}
	

}
