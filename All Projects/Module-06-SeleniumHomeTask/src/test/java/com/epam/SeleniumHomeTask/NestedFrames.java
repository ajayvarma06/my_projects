package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NestedFrames {

	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/nested_frames");
		driver.manage().window().maximize();
	}
	
	@Test
	public void testNestedFrames() {
		
		driver.switchTo().frame("frame-top");
		driver.switchTo().frame("frame-left");
		WebElement framesElement=driver.findElement(By.tagName("body"));
		System.out.println(framesElement.getText());
		
		driver.switchTo().parentFrame();
		driver.switchTo().frame("frame-middle");
		WebElement frameMiddle=driver.findElement(By.tagName("body"));
		System.out.println(frameMiddle.getText());
		
		driver.switchTo().parentFrame();
		driver.switchTo().frame("frame-right");
		WebElement frameRight=driver.findElement(By.tagName("body"));
		System.out.println(frameRight.getText());
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("frame-bottom");
		WebElement frameBottom=driver.findElement(By.tagName("body"));
		System.out.println(frameBottom.getText());
		
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}
}
