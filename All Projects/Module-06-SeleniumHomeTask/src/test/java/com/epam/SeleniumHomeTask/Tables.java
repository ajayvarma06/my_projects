package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Tables {

	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/tables");
		driver.manage().window().maximize();	
	}
	
	@Test
	public void testTables() {
		
	//	driver.findElement(By.xpath("//table[@id=\"table1\"]/tbody/tr[1]/td[6]/a[1]")).click();
	//	driver.findElement(By.xpath("//table[@id=\"table1\"]/tbody/tr[1]/td[6]/a[2]")).click();
		WebElement tableContent=driver.findElement(By.xpath("//table[@id=\"table1\"]"));
		System.out.println(tableContent.getText());
		System.out.println();
		WebElement tableContent2=driver.findElement(By.xpath("//table[@id=\"table2\"]"));
		System.out.println(tableContent2.getText());
		
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}

}
