package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Actions;
class Action {
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("https://www.youidraw.com/apps/painter/");
		driver.manage().window().maximize();	
	}
	@Test
	public void testActions()  {
		driver.findElement(By.id("pencil")).click();
        Actions actions = new Actions(driver);
        WebElement canvas = driver.findElement(By.id("catch"));
        actions.moveToElement(canvas,50,50).clickAndHold()
                .moveByOffset(50,-50)
                .moveByOffset(45,23)
                .moveByOffset(50,0)
                .moveByOffset(0,50)
                .release().perform();
		
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}

}
