package com.epam.SeleniumHomeTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Iframes {
	
	public WebDriver driver;

	@BeforeTest
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver = new ChromeDriver(options);
		driver.get("http://the-internet.herokuapp.com/tinymce");
		driver.manage().window().maximize();	
	}

	@Test
	public void testIframe() {
		
		driver.switchTo().frame("mce_0_ifr");
		WebElement iframeElement=driver.findElement(By.xpath("//body[@id='tinymce']/p"));
		iframeElement.clear();
		iframeElement.sendKeys("Selenium Task");
	}
	@AfterTest
	public void afterTest() {
		driver.close();
	}
}
