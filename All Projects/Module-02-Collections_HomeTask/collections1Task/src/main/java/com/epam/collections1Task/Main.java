package com.epam.collections1Task;


import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        List<Integer> numberList = CollectionsUtils.getNumberList();
        System.out.println("Number List: " + numberList);
        System.out.println("Second Largest Number: " + CollectionsUtils.findSecondLargestNumber(numberList));
        
        System.out.println("Sorted Numbers in Reverse Order: " + CollectionsUtils.sortNumbersInReverseOrder(numberList));

        List<String> employeeList = CollectionsUtils.getStringList();
        System.out.println("Employee List: " + employeeList);
        System.out.println("Sorted Employees by Name: " + CollectionsUtils.sortEmployeesByName(employeeList));

        Set<Integer> numberSet = CollectionsUtils.createSetOfNumbersSortedInReverseOrder();
        System.out.println("Number Set: " + numberSet);

        Set<String> employeeSet = CollectionsUtils.createSetOfEmployeesSortedByName();
        System.out.println("Employee Set: " + employeeSet);

        Map<Integer, String> treeMap = CollectionsUtils.createTreeMapSortedInDescendingOrder();
        System.out.println("TreeMap Sorted in Descending Order: " + treeMap);

        Map<Integer, String> employeeTreeMap = CollectionsUtils.createTreeMapSortedByEmployeeNameDescending();
        System.out.println("Employee TreeMap Sorted by Name in Descending Order: " + employeeTreeMap);

        List<String> sortedEmployeeList = CollectionsUtils.sortEmployeeListByNameDescending();
        System.out.println("Employee List Sorted by Name in Descending Order: " + sortedEmployeeList);
    }
}