package com.epam.collections1Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class CollectionsUtils {

	public static List<Integer> getNumberList() {
		List<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(5);
		list.add(1);
		list.add(50);
		return list;
	}

	public static List<String> getStringList() {
		List<String> list = new ArrayList<>();
		list.add("Ajay");
		list.add("Bharat");
		list.add("Priya");
		list.add("Charan");
		list.add("Bansile");
		return list;
	}

	public static Set<String> getStringSet() {
		Set<String> set = new TreeSet<>();
		set.add("Ajay");
		set.add("Bharat");
		set.add("Priya");
		set.add("Charan");
		set.add("Bansile");
		return set;
	}

	public static Map<Integer, String> getTreeMap() {
		Map<Integer, String> map = new TreeMap<>();
		map.put(1, "Ajay");
		map.put(12, "Bharat");
		map.put(1234, "Priya");
		map.put(12345, "Charan");
		map.put(123, "Bansile");
		return map;
	}

// Task 1: Find the second biggest number in the given list of numbers
	public static int findSecondLargestNumber(List<Integer> list) {
		if (list.size() < 2) {
			throw new IllegalArgumentException("List must have at least 2 elements");
		}
		List<Integer> sortedList = new ArrayList<>(list);
		Collections.sort(sortedList);
		return sortedList.get(sortedList.size() - 2);
	}

// Task 2: Use Comparator interface to sort given list of numbers in reverse order
	public static List<Integer> sortNumbersInReverseOrder(List<Integer> list) {
		List<Integer> sortedList = new ArrayList<>(list);
//	        sortedList.sort(Comparator.reverseOrder());
//	        return sortedList;
		Collections.sort(sortedList, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		return sortedList;
	}

// Task 3: Use Comparator interface to sort given list of employees in the alphabetical order of their name
	public static List<String> sortEmployeesByName(List<String> list) {
		List<String> sortedList = new ArrayList<>(list);
//	        sortedList.sort(String::compareTo);
//	        return sortedList;
		Collections.sort(sortedList, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		return sortedList;
	}

// Task 4: Create a TreeSet that sorts the given set of numbers in reverse order
	public static Set<Integer> createSetOfNumbersSortedInReverseOrder() {
		Set<Integer> set = new TreeSet<>(Collections.reverseOrder());
		set.add(1);
		set.add(50);
		set.add(10);
		set.add(5);
		set.add(25);
		return set;
	}

// Task 5: Create a TreeSet that sorts the given set of employees in alphabetical order of their name
	public static Set<String> createSetOfEmployeesSortedByName() {
		Set<String> set = getStringSet();
		return set;
	}

// Task 6: Create a TreeMap that sorts the given set of values in descending order
	public static Map<Integer, String> createTreeMapSortedInDescendingOrder() {
		Map<Integer, String> map = getTreeMap();
		Map<Integer, String> sortedMap = new TreeMap<>(Collections.reverseOrder());
		sortedMap.putAll(map);
		return sortedMap;
	}

// Task 7: Create a TreeMap that sorts the given set of employees in descending order of their name
	public static Map<Integer, String> createTreeMapSortedByEmployeeNameDescending() {
		Map<Integer, String> map = getTreeMap();
		List<Map.Entry<Integer, String>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
		Map<Integer, String> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<Integer, String> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

// Task 8: Use Collections.sort to sort the given list of employees in descending order of their name
	public static List<String> sortEmployeeListByNameDescending() {
		List<String> employeeList = getStringList();
//		Collections.sort(employeeList, Collections.reverseOrder());
//		return employeeList;
		Collections.sort(employeeList, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		return employeeList;
	}
}
