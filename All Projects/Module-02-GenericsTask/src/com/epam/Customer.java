package com.epam;

public class Customer implements Comparable<Customer> {

	private String customerName;
	private double customerSalary;

	public Customer(String customerName, double customerSalary) {
		super();
		this.customerName = customerName;
		this.customerSalary = customerSalary;
	}

	public String getCustomerName() {
		return customerName;
	}

	public double getCustomerSalary() {
		return customerSalary;
	}

	@Override
	public int compareTo(Customer compare) {
		
		if(this.customerSalary>compare.customerSalary)
			return 1;
		else if (this.customerSalary==compare.customerSalary)
			return 0;
		else
			return -1;
	}
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", customerSalary=" + customerSalary + "]";
	}

}
