package com.epam.HomeTask1.SelfDefs;

import static org.testng.Assert.assertEquals;
import com.epam.HomeTask1.Classes.Account;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class WithdrawCashStepDefs {
	
	private Account account;
	private int amountRequested;
	private int amountDispensed;
	
	@Given("I have a balance of ${int} in my account")
	public void iHaveABalanceOfInMyAccount(int balance) {
		account=new Account(balance);	
	}

	@When("I request ${int}")
	public void iRequest(int amount) {
		amountRequested=amount;
		amountDispensed=account.withdraw(amountRequested);
	}

	@Then("${int} should be dispensed")
	public void shouldBeDispensed(int expectedAmount) {
		assertEquals(expectedAmount, amountDispensed);
	}

}
