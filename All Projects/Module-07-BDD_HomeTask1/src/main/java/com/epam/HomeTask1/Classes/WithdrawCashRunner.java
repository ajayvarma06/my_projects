package com.epam.HomeTask1.Classes;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
glue = "com.epam.HomeTask1.SelfDefs",
features = {"src/test/Resources/features/withdrawCash.feature"})
public class WithdrawCashRunner extends AbstractTestNGCucumberTests {
	
}
