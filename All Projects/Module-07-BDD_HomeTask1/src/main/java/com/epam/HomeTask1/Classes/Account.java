package com.epam.HomeTask1.Classes;

public class Account {
	
	private int balance;
	public Account(int balance) {
		this.balance=balance;
	}
	
	public int withdraw(int amount) {
		if(balance>=amount) {
			balance=balance-amount;
			return amount;
		}
		else {
			return 0;
		}
	}
	
	public int getBalance() {
		return balance;
	}
}
