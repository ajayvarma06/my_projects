package com.epam.CoreJavaOOPS;
import java.util.*;

public class main_class_CoreJava {
	public static void main(String[] args){
		Address emp1Address=new Address(2,"NVVNagar","Hyderabad","Telangana","India");
		Course course=new Course(1,"JAVA",30);
		Employee e1=new Employee(101,"Ajay",1000,emp1Address);
		e1.printDetails();
		System.out.println("---------------------------------------------------------");

		Address emp2Address=new Address(4, "ABNagar","Hyderabad" , "Telangana", "India");
		JuniorEngineer e2 =new JuniorEngineer(110, "Shiva", 500, emp2Address, 80, "Good");
		e2.printDetails();
		System.out.println("---------------------------------------------------------");
		
		Address emp3Address=new Address(8, "PQNagar","Secunderabad" , "Telangana", "India");
		SoftwareEngineer e3 =new SoftwareEngineer(129, "Priya", 700, emp3Address, "BioTest");
		e3.printDetails();
		System.out.println("---------------------------------------------------------");
		
		Address emp4Address=new Address(8, "qwertyNagar","Medchal" , "Telangana", "India");
		Trainer e4 =new Trainer(185, "Arjun", 900, emp4Address, "Java,Python" , "CoreJava,Python");
		e4.printDetails();
		System.out.println("---------------------------------------------------------");

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter day:");
		String day= sc.nextLine();
		System.out.println("Enter k:");
		int k= sc.nextInt(); 
		String fDay=Task5.findDay(day,k);
		System.out.println(fDay);
		
		Task6.dupliCheck();
		sc.close();  
		}
}
