package com.epam.CoreJavaOOPS;

public class JuniorEngineer extends Employee
{
	int assessmentScore;
	String feedback;
	JuniorEngineer(int id, String name,double salary,Address address,int assessmentScore,String feedback)
	{
		super(id,name,salary,address);
		this.assessmentScore=assessmentScore;
		this.feedback=feedback;
	}
	public int getAssessmentScore()
	{
		return assessmentScore;
	}
	public String getFeedback()
	{
		return feedback;
	}
	@Override
	public void printDetails() {
		super.printDetails();
		System.out.println("assessmentScore : " + assessmentScore + ", feedback : " + feedback );
	}
	
	


}
