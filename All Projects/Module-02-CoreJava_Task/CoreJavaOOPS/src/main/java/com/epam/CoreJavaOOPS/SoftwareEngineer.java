package com.epam.CoreJavaOOPS;

public class SoftwareEngineer extends Employee
{
	private String projectName;
	SoftwareEngineer(int id, String name,double salary,Address address,String projectName)
	{
		super(id,name,salary,address);
		this.projectName=projectName;
	}
	public String getProjectName()
	{
		return projectName;
	}
	public void printDetails()
	{
		super.printDetails();
		System.out.println("projectName : "+getProjectName());
	}


}
