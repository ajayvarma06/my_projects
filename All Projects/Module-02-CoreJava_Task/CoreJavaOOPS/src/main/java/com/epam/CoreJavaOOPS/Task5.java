package com.epam.CoreJavaOOPS;

public class Task5 {
	public static String findDay(String day,int k){
		String[] days= {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
		int currentDay=0;
		for(int i=0;i<days.length;i++) {
			if(days[i].equals(day)) {
				currentDay=i;
				break;
			}
		}
		int futureDay=(currentDay+k)%7;
		return days[futureDay];
	}

}
