package com.epam.CoreJavaOOPS;

public class Trainer extends Employee
{
	private String skills;
	private String certifications;
	Trainer(int id, String name,double salary,Address address,String skills,String certifications)
	{
		super(id,name,salary,address);
		this.skills=skills;
		this.certifications=certifications;
	}
	public String getSkills()
	{
		return skills;
	}
	public String getCertifications()
	{
		return certifications;
	}
	@Override
	public void printDetails()
	{
		super.printDetails();
		System.out.println("skills: "+skills);
		System.out.println("certifications: "+ certifications);
	}
	
	
}
