package com.epam.CleanCode.After.automation.model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Vitali_Shulha on 22-Oct-15.
 */
public class HomePage {

    public static final String GITHUB_BASE_URL = "http://www.github.com";

    @FindBy(xpath = "//button[@aria-label='Switch account context']/span")
    private WebElement loggedInUserNamElement;

    private final WebDriver driver;

    /*
    Constructor
     */
    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get(GITHUB_BASE_URL);
        PageFactory.initElements(driver, this);
    }

    public String getLoggedInUserName() {
        return loggedInUserNamElement.getText();
    }

    public boolean hasUserLoggedIn(){
       return loggedInUserNamElement.isDisplayed();
    }
}

