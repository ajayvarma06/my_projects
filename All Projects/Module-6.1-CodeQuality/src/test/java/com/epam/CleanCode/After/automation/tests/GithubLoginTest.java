package com.epam.CleanCode.After.automation.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.epam.CleanCode.After.automation.pages.SignInPage;
import com.epam.CleanCode.After.automation.pages.StartPage;
import com.epam.CleanCode.After.automation.model.HomePage;

import java.time.Duration;

/**
 * Created by Vitali_Shulha on 22-Oct-15.
 */
public class GithubLoginTest //Class Name is not descriptive
{

    private final String USERNAME = "testautomationuser";
    private final String PASSWORD = "Time4Death!";
    
    private WebDriver driver;
    @BeforeTest
    public void setUp() {
    	driver = new FirefoxDriver();
    	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
	}

    @Test
    public void testOneCanLoginGithub(){
        StartPage startPage = new StartPage(driver);
        startPage.open();
        SignInPage signInPage = startPage.invokeSignIn();
        HomePage homePage = signInPage.signIn(USERNAME, PASSWORD);
        String loggedInUserName = homePage.getLoggedInUserName();
        Assert.assertEquals(USERNAME, loggedInUserName);
    }
    @AfterTest
    public void closeBrowser()
    {
        driver.quit();
    }

}
