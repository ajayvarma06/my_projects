package com.epam.CleanCode.Before.automation.pages;

import com.epam.CleanCode.Before.automation.model.HomePage;
import com.epam.CleanCode.Before.automation.model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Vitali_Shulha on 22-Oct-15.
 * Changes:
 * 19 May 2017 - now login returns HomePage class
 * 12 May 2017 - make login work with User object
 */
public class SignInPage {

    public static final String GITHUB_BASE_URL = "http://www.github.com";

    @FindBy(id = "login_field")
    private WebElement input1;//input1 is not properly Descriptive
    

    @FindBy(id = "password")
    private WebElement input2;//input2 is not properly Descriptive
    

    @FindBy(xpath = "//input[@value='Sign in']") //CSS Selector Can be used
    private WebElement buttonSubmit; //variable can be more descriptive

    private WebDriver mDriver;//mDriver : Naming Convention Mistake
     

    public SignInPage(WebDriver mDriver) {
        this.mDriver = mDriver;
        PageFactory.initElements(mDriver, this);
    }

    public HomePage signIn(String username, String password){
        input1.sendKeys(username);
        input2.sendKeys(password);
        buttonSubmit.click();
        return new HomePage(mDriver);
    }

    public boolean isReadyToSignIn() throws Exception //Method not Throwing any Checked Exceptions
    {
        if (buttonSubmit.isDisplayed()){
            return true;
        }
        else {
            throw new Exception();
            //Using More Descriptive Exception Type
        }
    }

    public void open() //Unused Method in the Class
    { 
        mDriver.get(GITHUB_BASE_URL);
    }

    public HomePage signIn(User user) //Error Handling can be Added in case of Wrong Credentials(signIn fail)
    {
        return this.signIn(user.getUsername(), user.getPassword());
    }

}
