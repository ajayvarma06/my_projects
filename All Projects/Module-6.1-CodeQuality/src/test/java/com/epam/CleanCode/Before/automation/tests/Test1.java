package com.epam.CleanCode.Before.automation.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.epam.CleanCode.Before.automation.model.HomePage;
import com.epam.CleanCode.Before.automation.pages.SignInPage;
import com.epam.CleanCode.Before.automation.pages.StartPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by Vitali_Shulha on 22-Oct-15.
 */
public class Test1 //Class Name is not descriptive
{

    private final String USERNAME = "testautomationuser";
    private final String PASSWORD = "Time4Death!";
    
    @Test
    public void testOneCanLoginGithub(){
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        StartPage startPage = new StartPage(driver);
        startPage.open();
        SignInPage signInPage = startPage.invokeSignIn();
        HomePage homePage = signInPage.signIn(USERNAME, PASSWORD);
        String loggedInUserName = homePage.getLoggedInUserName();
        Assert.assertEquals(USERNAME, loggedInUserName);
        driver.quit();
        
        // BeforeTest and AfterTest annotation Methods should be used
        //Error Handling Can be Added for Test Fail 
    }
}
