package com.epam.CleanCode.After.automation.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.epam.CleanCode.After.automation.model.HomePage;
import com.epam.CleanCode.After.automation.model.User;

/**
 * Created by Vitali_Shulha on 22-Oct-15.
 * Changes:
 * 19 May 2017 - now login returns HomePage class
 * 12 May 2017 - make login work with User object
 */
public class SignInPage {

    public static final String GITHUB_BASE_URL = "http://www.github.com";

    @FindBy(id = "login_field")
    private WebElement usernameInput;
    

    @FindBy(id = "password")
    private WebElement passwordInput;
    

    @FindBy(xpath = "//input[@value='Sign in']") //CSS Selector Can be used
    private WebElement signInButton;

    private WebDriver driver;
     

    public SignInPage(WebDriver mDriver) {
        this.driver = mDriver;
        PageFactory.initElements(mDriver, this);
    }

    public HomePage signIn(String username, String password){
    	usernameInput.sendKeys(username);
    	passwordInput.sendKeys(password);
    	signInButton.click();
        return new HomePage(driver);
    }

    public boolean isReadyToSignIn()
    {
        if (signInButton.isDisplayed()){
            return true;
        }
        else {
            throw new ElementNotInteractableException("Element Not Displayed");
        }
    }

 
    public HomePage signIn(User user) 
    {
        return this.signIn(user.getUsername(), user.getPassword());
    }

}

